 
# Advent of code solutions

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

The code is located in the [src](./src) folder

## Running

Create a virtual environment:

    python -m venv venv
    source venv/bin/activate
    python -m pip install -r requirements.txt

Create an ``.env`` file with the following content:

    export SESSION_ID="<your_session_id_from_browser>"

Assuming a still active ``venv`` each file can be executed via

    cd src/
    python d04/p04.py

The tests can be run with (from outside ``src/``):

    pytest --cov=src/ src/
