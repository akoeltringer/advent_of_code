# -*- coding: utf-8 -*-

# stdlib imports
import collections
from typing import List

# 3rd party lib imports
import pytest

# own stuff
from .p23 import (
    play_game,
    play_game_efficient,
    get_order_after_1,
    play_game_more_efficient,
    create_cups_dict,
)


cups = [3, 8, 9, 1, 2, 5, 4, 6, 7]
first_10_rounds = [
    (1, [2, 8, 9, 1, 5, 4, 6, 7, 3]),
    (2, [5, 4, 6, 7, 8, 9, 1, 3, 2]),
    (3, [8, 9, 1, 3, 4, 6, 7, 2, 5]),
    (4, [4, 6, 7, 9, 1, 3, 2, 5, 8]),
    (5, [1, 3, 6, 7, 9, 2, 5, 8, 4]),
    (6, [9, 3, 6, 7, 2, 5, 8, 4, 1]),
    (7, [2, 5, 8, 3, 6, 7, 4, 1, 9]),
    (8, [6, 7, 4, 1, 5, 8, 3, 9, 2]),
    (9, [5, 7, 4, 1, 8, 3, 9, 2, 6]),
    (10, [8, 3, 7, 4, 1, 9, 2, 6, 5]),
]


@pytest.mark.parametrize("n_rounds,expected", first_10_rounds)
def test_play_game(n_rounds: int, expected: List[int]):
    assert play_game(cups, n_rounds) == expected


def test_get_order_after_1():
    assert get_order_after_1(play_game(cups, 100)) == "67384529"


@pytest.mark.parametrize("n_rounds,expected", first_10_rounds)
def test_play_game_efficient(n_rounds: int, expected: List[int]):
    assert list(play_game_efficient(collections.deque(cups), n_rounds)) == expected


@pytest.mark.parametrize("n_rounds,expected", first_10_rounds)
def test_play_game_more_efficient(n_rounds: int, expected: List[int]):
    assert play_game_more_efficient(cups, n_rounds) == create_cups_dict(expected)[1]
