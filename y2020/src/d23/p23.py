#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 23"""

# stdlib imports
from typing import Deque, Dict, List, Tuple, Union

# 3rd party lib imports

# own stuff


def find_ins_pos(cur: int, rest: Union[List[int], Deque[int]]) -> int:
    """find the position where to insert the next three elements"""
    while cur > 0:
        cur -= 1
        if cur in rest:
            return rest.index(cur) + 1

    return rest.index(max(rest)) + 1


def play_game(cups: List[int], n_turns: int) -> List[int]:
    """play the game by simple list manipulations. Works but does not scale (part 2)"""
    for _ in range(n_turns):

        cur, nxt3, rest = cups[0], cups[1:4], cups[4:]
        ins_pos = find_ins_pos(cur, rest)

        cups = rest[:ins_pos] + nxt3 + rest[ins_pos:] + [cur]

    return cups


def play_game_efficient(cups: Deque[int], n_turns: int) -> Deque[int]:
    """use a more efficient approach: using deque instead of lists
    ... still not good enough for part 2 though.
    """
    for _ in range(n_turns):

        cur = cups.popleft()
        nxt3 = [cups.popleft() for _ in range(3)]
        ins_pos = find_ins_pos(cur, cups)
        for i, val in enumerate(nxt3):
            cups.insert(ins_pos + i, val)
        cups.append(cur)

    return cups


def find_ins_value(cur: int, cups: Dict[int, int], ignore: List[int]) -> int:
    """find the value where to insert "the next three" """
    # counting down until finding a lower cup in the list
    while cur > 0:
        cur -= 1
        if cur in cups and cur not in ignore:
            return cur

    # if no lower cup was found, wrap around and start at the highest
    cur = max(cups)
    while cur in ignore:
        cur -= 1

    return cur


def create_cups_dict(cups_list: List[int]) -> Tuple[int, Dict[int, int]]:
    """create a dictionary, where each value maps to its neighbor (clockwise)"""
    current_elem = cups_list[0]
    # convert to dict:
    # - the keys are the cup values
    # - the values are the next item
    cups = {k: v for k, v in zip(cups_list, cups_list[1:] + [current_elem])}

    return current_elem, cups


def play_game_more_efficient(cups_list: List[int], n_turns: int) -> Dict[int, int]:
    """play the game: use a "linked value" approach with a dictionary"""
    current_elem, cups = create_cups_dict(cups_list)

    for _ in range(n_turns):

        one_of_3 = cups[current_elem]
        two_of_3 = cups[one_of_3]
        three_of_3 = cups[two_of_3]
        after_3 = cups[three_of_3]

        ins_val = find_ins_value(current_elem, cups, [one_of_3, two_of_3, three_of_3])

        # insert 3 vals
        after_ins_val = cups[ins_val]
        cups[ins_val] = one_of_3
        cups[three_of_3] = after_ins_val

        # remove 3 vals
        cups[current_elem] = after_3
        current_elem = after_3

    return cups


def get_order_after_1(cups: List[int]) -> str:
    """return the elements clockwise after (and excluding) the ``1``"""
    pos_one = cups.index(1)
    others = cups[(pos_one + 1) :] + cups[:pos_one]
    return "".join([str(elem) for elem in others])


def get_first_two_after_1(cups: Dict[int, int]) -> int:
    """get the product of the first two elements after ``1``"""
    first = cups[1]
    second = cups[first]
    return first * second


def main() -> None:
    cups = [1, 2, 3, 4, 8, 7, 5, 9, 6]

    # part 1
    labels_cups = get_order_after_1(play_game(cups, 100))
    print(f"part 1: {labels_cups}")

    # part 2
    cups_2 = cups + list(range(max(cups) + 1, 1_000_001))
    cup_mult = get_first_two_after_1(play_game_more_efficient(cups_2, 10_000_000))
    print(f"part 2: {cup_mult}")


if __name__ == "__main__":
    main()
