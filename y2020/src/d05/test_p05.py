# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p05 import get_seat_id, zone_to_seat


@pytest.mark.parametrize(
    "zone,row,col",
    [
        ("FBFBBFFRLR", 44, 5),
        ("BFFFBBFRRR", 70, 7),
        ("FFFBBBFRRR", 14, 7),
        ("BBFFBBFRLL", 102, 4),
    ],
)
def test_zone_to_seat(zone, row, col):
    assert zone_to_seat(zone) == (row, col)


@pytest.mark.parametrize(
    "row,col,expected",
    [(44, 5, 357), (70, 7, 567), (14, 7, 119), (102, 4, 820)],
)
def test_get_seat_id(row, col, expected):
    assert get_seat_id((row, col)) == expected
