#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 19"""

# stdlib imports
import re
from typing import Dict, List, Union

# 3rd party lib imports

# own stuff
import utils

T_TREE = Dict[int, List[List[Union[int, str]]]]


def rules_to_tree(input_raw: str) -> T_TREE:
    rules = {}
    for rule_raw in input_raw.split("\n"):
        k, vals_raw = rule_raw.split(": ")

        vals = []
        for sub in vals_raw.split(" | "):
            subsub = sub.split(" ")
            subsub = [elem.replace("\"", "") if elem.startswith("\"") else int(elem) for elem in subsub]
            vals.append(subsub)

        rules[int(k)] = vals

    return rules


def to_regex(tree: Dict[int, List[List[Union[int, str]]]]) -> str:

    def recurse(elem: int) -> str:
        return "({})".format(
            "|".join(["".join([recurse(i) if isinstance(i, int) else i for i in sub]) for sub in tree[elem]])
        )

    return recurse(0)


def count_matching(rules_raw: str, text_raw: str) -> int:

    rules_tree = rules_to_tree(rules_raw)
    pattern_str = to_regex(rules_tree)
    print(len(pattern_str))
    pattern = re.compile(pattern_str)

    n = 0
    for line in text_raw.split("\n"):
        if pattern.fullmatch(line):
            n += 1

    return n


def main() -> None:
    rules_raw, text_raw = utils.get_input(__file__).strip().split("\n\n")

    # part 1
    n_matching = count_matching(rules_raw, text_raw)
    print(f"part 1: {n_matching}")


if __name__ == "__main__":
    main()
