# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports

# own stuff
from .p19 import rules_to_tree, count_matching


rules_raw = """
0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: "a"
5: "b"
"""

text_raw = """
ababbb
bababa
abbbab
aaabbb
aaaabbb
"""


def test_rules_to_tree():
    assert rules_to_tree(rules_raw.strip()) == {
        0: [[4, 1, 5]],
        1: [[2, 3], [3, 2]],
        2: [[4, 4], [5, 5]],
        3: [[4, 5], [5, 4]],
        4: [["a"]],
        5: [["b"]],
    }


def test_count_matching():
    assert count_matching(rules_raw, text_raw.strip()) == 2
