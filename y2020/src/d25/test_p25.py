# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p25 import find_loop_size, find_enc_key


@pytest.mark.parametrize("pkey,loop_size", [(5764801, 8), (17807724, 11)])
def test_find_loop_size(pkey, loop_size):
    assert find_loop_size(pkey) == loop_size


def test_find_enc_key():
    assert find_enc_key(5764801, 17807724) == 14897079
