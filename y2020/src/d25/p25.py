#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 25"""

# stdlib imports

# 3rd party lib imports

# own stuff



loop_vals = {1: 0}  # start with a value of 1 (at loop "zero")


def find_loop_size(val: int) -> int:
    subj_num = 7

    if val in loop_vals:
        return loop_vals[val]

    i = max(loop_vals.values())
    next_val = max(loop_vals.keys(), key=lambda x: loop_vals[x])
    while True:
        i += 1
        next_val = (next_val * subj_num) % 20201227
        loop_vals[next_val] = i

        if next_val == val:
            return i


def transform_subj_num(subj_num, n_times):
    value = 1
    for _ in range(n_times):
        value = (value * subj_num) % 20201227
    return value


def find_enc_key(pkey_card, pkey_door) -> int:
    loop_size_card = find_loop_size(pkey_card)
    loop_size_door = find_loop_size(pkey_door)
    enc_key_card = transform_subj_num(pkey_door, loop_size_card)
    enc_key_door = transform_subj_num(pkey_card, loop_size_door)

    if enc_key_door != enc_key_card:
        raise ValueError("enc keys do not match")

    return enc_key_door


def main() -> None:
    pkey_card, pkey_door = 10604480, 4126658

    # part 1
    enc_key = find_enc_key(pkey_card, pkey_door)
    print(f"part 1: {enc_key}")

    # part 2


if __name__ == "__main__":
    main()
