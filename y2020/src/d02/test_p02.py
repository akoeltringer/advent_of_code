# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p02 import Password


@pytest.mark.parametrize(
    "password,expected",
    [("1-3 a: abcde", True), ("1-3 b: cdefg", False), ("2-9 c: ccccccccc", True)],
)
def test_password_is_valid_old(password, expected):
    assert Password(password).is_valid_old() == expected


@pytest.mark.parametrize(
    "password,expected",
    [("1-3 a: abcde", True), ("1-3 b: cdefg", False), ("2-9 c: ccccccccc", False)],
)
def test_password_is_valid(password, expected):
    assert Password(password).is_valid() == expected
