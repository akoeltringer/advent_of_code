# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p14 import (
    get_apply_mask_value_func,
    value_decoder,
    get_apply_mask_addr_func,
    memory_address_decoder,
)


MASK1 = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"


@pytest.mark.parametrize(
    "mask_raw,input_value,output_value",
    [(MASK1, 11, 73), (MASK1, 101, 101), (MASK1, 0, 64)],
)
def test_get_apply_mask_value_func(mask_raw: str, input_value: int, output_value: int):
    f = get_apply_mask_value_func(mask_raw)
    assert f(input_value) == output_value


def test_value_decoder():
    sample_program = [
        "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X",
        "mem[8] = 11",
        "mem[7] = 101",
        "mem[8] = 0",
    ]
    assert value_decoder(sample_program) == 165


@pytest.mark.parametrize(
    "mask_raw,input_value,output_value",
    [
        (
            "000000000000000000000000000000X1001X",
            42,
            [26, 27, 58, 59],
        ),
        (
            "00000000000000000000000000000000X0XX",
            26,
            [16, 17, 18, 19, 24, 25, 26, 27],
        ),
    ],
)
def test_get_apply_mask_addr_func(mask_raw: str, input_value: int, output_value: int):
    f = get_apply_mask_addr_func(mask_raw)
    assert f(input_value) == output_value


def test_memory_address_decoder():
    sample_program = [
        "mask = 000000000000000000000000000000X1001X",
        "mem[42] = 100",
        "mask = 00000000000000000000000000000000X0XX",
        "mem[26] = 1",
    ]
    assert memory_address_decoder(sample_program) == 208
