# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p09 import parse_input, find_first_invalid_xmas_num, find_encryption_weakness


xmas_nums_raw = """
35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576
"""


def test_find_first_invalid_xmas_num():
    assert find_first_invalid_xmas_num(parse_input(xmas_nums_raw), 5) == 127


def test_find_encryption_weakness():
    assert find_encryption_weakness(parse_input(xmas_nums_raw), 127) == 62
