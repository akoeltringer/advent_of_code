# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports

# own stuff
from .p21 import (
    get_ingredients_without_allergenes,
    parse_input,
    map_allergenes_to_ingredients,
)


input_raw = """
mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)
"""


def test_get_ingredients_without_allergenes():
    assert get_ingredients_without_allergenes(*parse_input(input_raw)) == 5


def test_map_allergenes_to_ingredients():
    assert (
        map_allergenes_to_ingredients(parse_input(input_raw)[1])
        == "mxmxvkd,sqjhc,fvjkl"
    )
