#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 20"""

# stdlib imports
import copy
import itertools
import math
from typing import Dict, List

# 3rd party lib imports

# own stuff
import utils

T_TILE = List[List[int]]


def parse_input(input_raw: str) -> Dict[int, T_TILE]:

    tiles = {}

    for tile in input_raw.strip().split("\n\n"):
        lines_raw = tile.split("\n")
        tile_id = int(lines_raw[0][5:-1])

        lines = []
        for line in lines_raw[1:]:
            lines.append([1 if elem == "#" else 0 for elem in line])

        tiles[tile_id] = lines

    return tiles


def get_borders(tiles: Dict[int, T_TILE]) -> Dict[int, T_TILE]:

    tile_borders = {}

    for tile_id, tile_data in tiles.items():
        top, bottom = tile_data[0], tile_data[-1]
        left = [row[0] for row in tile_data]
        right = [row[-1] for row in tile_data]
        tile_borders[tile_id] = [top, right, bottom, left]

    return tile_borders


def match_stupid(tiles: Dict[int, T_TILE]) -> int:
    """try the stupid match: assume border "ids" are unique and no need to care for
    which border is on which side => can remove matching borders and in the end, the
    tiles with two borders left are the ones on the corners.
    """
    tiles = copy.deepcopy(tiles)

    for tile_id1, tile_id2 in itertools.combinations(tiles.keys(), 2):
        for border1 in tiles[tile_id1]:
            for border2 in tiles[tile_id2]:
                if border1 == border2 or border1 == list(reversed(border2)):
                    tiles[tile_id1].remove(border1)
                    tiles[tile_id2].remove(border2)

    corner_ids = [tile_id for tile_id in tiles if len(tiles[tile_id]) == 2]
    if len(corner_ids) != 4:
        raise ValueError("stupid approach did not work!")

    return math.prod(corner_ids)


def main() -> None:
    tiles = parse_input(utils.get_input(__file__))
    borders = get_borders(tiles)

    # part 1
    corner_prod = match_stupid(borders)
    print(f"part 1: {corner_prod}")

    # part 2


if __name__ == "__main__":
    main()
