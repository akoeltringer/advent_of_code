# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p11 import (
    compute_round_1,
    compute_round_2,
    compute_rounds,
    get_diag_1,
    get_diag_2,
    get_neighbor_coords,
    is_occupied,
    parse_grid,
)


round0 = """
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
"""

round1 = """
#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##
"""

p1round2 = """
#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##
"""

p1round3 = """
#.##.L#.##
#L###LL.L#
L.#.#..#..
#L##.##.L#
#.##.LL.LL
#.###L#.##
..#.#.....
#L######L#
#.LL###L.L
#.#L###.##
"""

p1round4 = """
#.#L.L#.##
#LLL#LL.L#
L.L.L..#..
#LLL.##.L#
#.LL.LL.LL
#.LL#L#.##
..L.L.....
#L#LLLL#L#
#.LLLLLL.L
#.#L#L#.##
"""

p1round5 = """
#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##
"""

p2round2 = """
#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#
"""

p2round3 = """
#.L#.##.L#
#L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
#.#####.#L
..#.#.....
LLL####LL#
#.L#####.L
#.L####.L#
"""

p2round4 = """
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
#.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLL#.L
#.L#LL#.L#
"""

p2round5 = """
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
#.L####.LL
..#.#.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
"""

p2round6 = """
#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
#.LLLL#.LL
..#.L.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
"""


@pytest.mark.parametrize(
    "y,x,height,width,expected",
    [
        (1, 1, 3, 3, (0, 2, 0, 2)),
        (0, 0, 3, 3, (0, 1, 0, 1)),
        (2, 2, 3, 3, (1, 2, 1, 2)),
    ],
)
def test_neighbor_coords(y, x, height, width, expected):
    assert get_neighbor_coords(y, x, height, width) == expected


@pytest.mark.parametrize(
    "round_before,round_after",
    [
        (round0, round1),
        (round1, p1round2),
        (p1round2, p1round3),
        (p1round3, p1round4),
        (p1round4, p1round5),
        (p1round5, p1round5),
    ],
)
def test_compute_round_1(round_before, round_after):
    round_before, round_after = parse_grid(round_before), parse_grid(round_after)
    assert compute_round_1(round_before) == round_after


@pytest.mark.parametrize(
    "round_before,round_after",
    [
        # (round0, round1),
        (round1, p2round2),
        (p2round2, p2round3),
        (p2round3, p2round4),
        (p2round4, p2round5),
        (p2round5, p2round6),
        (p2round6, p2round6),
    ],
)
def test_compute_round_2(round_before, round_after):
    round_before, round_after = parse_grid(round_before), parse_grid(round_after)
    assert compute_round_2(round_before) == round_after


@pytest.mark.parametrize(
    "compute_fun,expected", [(compute_round_1, 37), (compute_round_2, 26)]
)
def test_compute_rounds(compute_fun, expected):
    assert compute_rounds(parse_grid(round0), compute_fun) == expected


matrix = [
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 10, 11, 12]
]


@pytest.mark.parametrize(
    "y,x,expected", [(1, 0, [None, 5, 10]), (1, 2, [2, 7, 12]), (1, 3, [3, 8, None])]
)
def test_get_diag_1(y, x, expected):
    assert list(get_diag_1(matrix, y, x)) == expected


@pytest.mark.parametrize(
    "y,x,expected", [(0, 0, [1, None, None]), (1, 2, [4, 7, 10]), (1, 3, [None, 8, 11])]
)
def test_get_diag_2(y, x, expected):
    assert list(get_diag_2(matrix, y, x)) == expected


@pytest.mark.parametrize(
    "ahead,expected",
    [
        (["#"], True),
        (["#", "L"], True),
        (["L", "#"], False),
        (["L"], False),
        ([".", "L", "#"], False),
        ([".", "#", "L"], True),
    ],
)
def test_is_occupied(ahead, expected):
    assert is_occupied(ahead) == expected
