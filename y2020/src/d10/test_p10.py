# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p10 import (
    get_deltas,
    part1,
    count_arrangements_bf,
    count_arrangements_partition,
    get_partitions,
)


ex1 = [0, 1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19, 22]
ex2 = [0, 1, 2, 3, 4, 7, 8, 9, 10, 11, 14, 17, 18, 19, 20, 23, 24, 25, 28, 31, 32, 33, 34, 35, 38, 39, 42, 45, 46, 47, 48, 49, 52]


@pytest.mark.parametrize("input,expected", [(ex1, 35), (ex2, 220)])
def test_part1(input, expected):
    assert part1(input) == expected


@pytest.mark.parametrize("input,expected", [(ex1, 8), (ex2, 19208)])
def test_count_arrangements_bf(input, expected):
    assert count_arrangements_bf(input) == expected


@pytest.mark.parametrize("input,expected", [(ex1, 8), (ex2, 19208)])
def test_count_arrangements_partition(input, expected):
    assert count_arrangements_partition(input) == expected


@pytest.mark.parametrize(
    "input,expected", [([1, 2, 3], [1, 1]), ([1, 0, 1, 0], [-1, 1, -1])]
)
def test_get_deltas(input, expected):
    assert get_deltas(input) == expected


@pytest.mark.parametrize(
    "input,expected",
    [
        (ex1, [[5, 6], [11]]),
        (ex2, [[1, 2, 3], [8, 9, 10], [18, 19], [24], [32, 33, 34], [46, 47, 48]]),
    ],
)
def test_get_partitions(input, expected):
    assert get_partitions(input) == expected
