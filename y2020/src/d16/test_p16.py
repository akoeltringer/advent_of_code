# -*- coding: utf-8 -*-

# stdlib imports
import textwrap

# 3rd party lib imports

# own stuff
from .p16 import (
    Rule,
    parse_rules,
    parse_tickets,
    parse_input,
    find_valid_tickets,
    find_matching_rules,
    find_assignments,
)


def test_parse_rules():
    rules_raw = "class: 1-3 or 5-7\nrow: 6-11 or 33-44\nseat: 13-40 or 45-50"
    assert parse_rules(rules_raw) == [
        Rule("class", [range(1, 4), range(5, 8)]),
        Rule("row", [range(6, 12), range(33, 45)]),
        Rule("seat", [range(13, 41), range(45, 51)]),
    ]


def test_parse_my_ticket():
    ticket_raw = "your ticket:\n7,1,14"
    assert parse_tickets(ticket_raw) == [[7, 1, 14]]


def test_parse_other_tickets():
    tickets_raw = "nearby tickets:\n7,3,47\n40,4,50\n55,2,20\n38,6,12"
    assert parse_tickets(tickets_raw) == [
        [7, 3, 47],
        [40, 4, 50],
        [55, 2, 20],
        [38, 6, 12],
    ]


def test_find_valid_tickets():
    input_raw = textwrap.dedent(
        """
        class: 1-3 or 5-7
        row: 6-11 or 33-44
        seat: 13-40 or 45-50
        
        your ticket:
        7,1,14
        
        nearby tickets:
        7,3,47
        40,4,50
        55,2,20
        38,6,12
    """
    )
    rules, _, other_tickets = parse_input(input_raw)
    assert find_valid_tickets(rules, other_tickets)[1] == [4, 55, 12]


def test_find_assignments():
    input_raw = textwrap.dedent(
        """
        class: 0-1 or 4-19
        row: 0-5 or 8-19
        seat: 0-13 or 16-19
        
        your ticket:
        11,12,13
        
        nearby tickets:
        3,9,18
        15,1,5
        5,14,9
    """
    )
    rules, my_ticket, other_tickets = parse_input(input_raw)
    valid_tickets = other_tickets + [my_ticket]
    matches = find_matching_rules(rules, valid_tickets)

    assert find_assignments(matches) == {0: "row", 1: "class", 2: "seat"}
