# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p03 import walk_count_trees


GRID = [
    "..##.......",
    "#...#...#..",
    ".#....#..#.",
    "..#.#...#.#",
    ".#...##..#.",
    "..#.##.....",
    ".#.#.#....#",
    ".#........#",
    "#.##...#...",
    "#...##....#",
    ".#..#...#.#",
]


@pytest.mark.parametrize(
    "right,down,grid,expected",
    [
        (1, 1, GRID, 2),
        (3, 1, GRID, 7),
        (5, 1, GRID, 3),
        (7, 1, GRID, 4),
        (1, 2, GRID, 2),
    ],
)
def test_get_path_segment(right, down, grid, expected):
    assert walk_count_trees(right, down, grid) == expected
