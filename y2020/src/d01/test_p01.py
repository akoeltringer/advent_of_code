# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p01 import get_prod_2020


@pytest.mark.parametrize(
    "list_elems,expected", [([1721, 979, 366, 299, 675, 1456], 514579)]
)
def test_get_prod_2020_2(list_elems, expected):
    assert get_prod_2020(list_elems, 2) == expected


@pytest.mark.parametrize(
    "list_elems,expected", [([1721, 979, 366, 299, 675, 1456], 241861950)]
)
def test_get_prod_2020_3(list_elems, expected):
    assert get_prod_2020(list_elems, 3) == expected
