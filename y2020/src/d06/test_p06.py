# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p06 import count_any_question_yes, count_all_questions_yes


@pytest.mark.parametrize(
    "group_input,expected",
    [("abc", 3), ("a\nb\nc", 3), ("ab\nac", 3), ("a\na\na\na", 1), ("b", 1)],
)
def test_count_questions_yes(group_input, expected):
    assert count_any_question_yes(group_input) == expected


@pytest.mark.parametrize(
    "group_input,expected",
    [("abc", 3), ("a\nb\nc", 0), ("ab\nac", 1), ("a\na\na\na", 1), ("b", 1)],
)
def test_count_all_questions_yes(group_input, expected):
    assert count_all_questions_yes(group_input) == expected
