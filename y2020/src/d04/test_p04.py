# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p04 import (
    count_valid_passports,
    parse_passports,
    is_passport_valid_1,
    is_passport_valid_2,
    is_byr_valid,
    is_ecl_valid,
    is_hcl_valid,
    is_hgt_valid,
    is_pid_valid,
)


PASSPORTS_RAW_1 = """
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
"""

PASSPORTS_RAW_2_INVALID = """
eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
"""

PASSPORTS_RAW_2_VALID = """
pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
"""


def test_count_valid_passports_1():
    assert (
        count_valid_passports(is_passport_valid_1, parse_passports(PASSPORTS_RAW_1))
        == 2
    )


@pytest.mark.parametrize("byr,expected", [("2002", True), ("2003", False)])
def test_is_byr_valid(byr: str, expected: bool):
    assert is_byr_valid(byr) == expected


@pytest.mark.parametrize(
    "hgt,expected", [("60in", True), ("190cm", True), ("190in", False), ("190", False)]
)
def test_is_hgt_valid(hgt: str, expected: bool):
    assert is_hgt_valid(hgt) == expected


@pytest.mark.parametrize(
    "hcl,expected", [("#123abc", True), ("#123abz", False), ("123abc", False)]
)
def test_is_hcl_valid(hcl: str, expected: bool):
    assert is_hcl_valid(hcl) == expected


@pytest.mark.parametrize("ecl,expected", [("brn", True), ("wat", False)])
def test_is_ecl_valid(ecl: str, expected: bool):
    assert is_ecl_valid(ecl) == expected


@pytest.mark.parametrize("pid,expected", [("000000001", True), ("0123456789", False)])
def test_is_pid_valid(pid: str, expected: bool):
    assert is_pid_valid(pid) == expected


@pytest.mark.parametrize(
    "passports_raw,expected", [(PASSPORTS_RAW_2_VALID, 4), (PASSPORTS_RAW_2_INVALID, 0)]
)
def test_count_valid_passports_2(passports_raw, expected):
    assert (
        count_valid_passports(is_passport_valid_2, parse_passports(passports_raw))
        == expected
    )
