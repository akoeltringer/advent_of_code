# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports

# own stuff
from .p22 import parse_input, play_game, play_game_recursive


input_raw = """
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
"""


def test_parse_input():
    assert parse_input(input_raw) == ([9, 2, 6, 3, 1], [5, 8, 4, 7, 10])


def test_play_game():
    assert play_game(*parse_input(input_raw)) == 306


def test_play_game_recursive():
    assert play_game_recursive(*parse_input(input_raw)) == 291
