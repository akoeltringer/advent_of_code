# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports

# own stuff
from .p17 import parse_input, get_neighbors, compute_cycle


input_raw = """
.#.
..#
###
"""


def test_parse_input_3dim():
    assert parse_input(input_raw, 3) == {
        (1, 0, 0), (2, 1, 0), (0, 2, 0), (1, 2, 0), (2, 2, 0),
    }


def test_parse_input_4dim():
    assert parse_input(input_raw, 4) == {
        (1, 0, 0, 0), (2, 1, 0, 0), (0, 2, 0, 0), (1, 2, 0, 0), (2, 2, 0, 0),
    }


def test_get_neighbors():
    # fmt: off
    assert sorted(list(get_neighbors((1, 1, 1)))) == sorted([
        (0, 0, 0), (1, 0, 0), (2, 0, 0),
        (0, 1, 0), (1, 1, 0), (2, 1, 0),
        (0, 2, 0), (1, 2, 0), (2, 2, 0),
        (0, 0, 1), (1, 0, 1), (2, 0, 1),
        (0, 1, 1),            (2, 1, 1),
        (0, 2, 1), (1, 2, 1), (2, 2, 1),
        (0, 0, 2), (1, 0, 2), (2, 0, 2),
        (0, 1, 2), (1, 1, 2), (2, 1, 2),
        (0, 2, 2), (1, 2, 2), (2, 2, 2),
    ])


def test_compute_cycle_3dim():
    dim = parse_input(input_raw, 3)
    for _ in range(6):
        dim = compute_cycle(dim)

    assert len(dim) == 112


def test_compute_cycle_4dim():
    dim = parse_input(input_raw, 4)
    for _ in range(6):
        dim = compute_cycle(dim)

    assert len(dim) == 848
