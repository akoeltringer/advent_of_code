# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p08 import computer, parse_instructions


instructions = """
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
"""


def test_computer():
    assert computer(parse_instructions(instructions)) == (5, -1)
