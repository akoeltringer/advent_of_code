# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p13 import (
    compute_wait_time,
    parse_instructions,
    find_line_next_depart,
    find_earliest_tmsp_for_pattern_bf,
)


input_raw1 = """
939
7,13,x,x,59,x,31,19
"""

input_raw2 = """
0
17,x,13,19
"""

input_raw3 = """
0
67,7,59,61
"""

input_raw4 = """
0
67,x,7,59,61
"""

input_raw5 = """
0
67,7,x,59,61
"""

input_raw6 = """
0
1789,37,47,1889
"""


@pytest.mark.parametrize(
    "tmsp,bus_line,expected",
    [(939, 7, 6), (939, 13, 10), (939, 59, 5), (1068781, 7, 0)],
)
def test_compute_wait_time(tmsp: int, bus_line: int, expected: int):
    assert compute_wait_time(tmsp, bus_line) == expected


def test_find_line_next_depart():
    assert find_line_next_depart(*parse_instructions(input_raw1)) == 295


@pytest.mark.parametrize(
    "input_raw,expected",
    [
        (input_raw1, 1068781),
        (input_raw2, 3417),
        (input_raw3, 754018),
        (input_raw4, 779210),
        (input_raw5, 1261476),
        (input_raw6, 1202161486),
    ],
)
def test_find_earliest_tmsp_for_pattern_bf(input_raw, expected):
    assert (
        find_earliest_tmsp_for_pattern_bf(parse_instructions(input_raw)[1]) == expected
    )
