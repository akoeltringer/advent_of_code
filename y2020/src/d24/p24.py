#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 24"""

# stdlib imports
import copy
import dataclasses
import re
from typing import Set, List, Iterable

# 3rd party lib imports

# own stuff
import utils

MOVES_PATTERN = re.compile(r"nw|ne|e|se|sw|w")


# with eq=True and frozen=True, the dataclass generates a __hash__ method which
# is required, b/c Point2D instances will be stored in sets
@dataclasses.dataclass(eq=True, frozen=True)
class Point2D:
    x: int
    y: int

    def __add__(self, other: "Point2D"):
        return Point2D(self.x + other.x, self.y + other.y)


# deltas for moving on the grid
deltas = {
    "nw": Point2D(0, -1),
    "ne": Point2D(1, -1),
    "e": Point2D(1, 0),
    "se": Point2D(0, 1),
    "sw": Point2D(-1, 1),
    "w": Point2D(-1, 0),
}


def flip_tile(moves_str: str, grid: Set[Point2D]):
    """flip a tile according to a moves str"""
    moves = MOVES_PATTERN.findall(moves_str)
    p = Point2D(0, 0)

    for move in moves:
        p += deltas[move]

    if p in grid:
        grid.remove(p)
    else:
        grid.add(p)


def lay_out_grid(moves_str: str) -> Set[Point2D]:
    """lay out the grid according to the moves"""
    grid: Set[Point2D] = set()
    for move in moves_str.strip().split("\n"):
        flip_tile(move, grid)

    return grid


def get_neighbors(point: Point2D) -> List[Point2D]:
    """get the neighbors of a point"""
    return [point + d for d in deltas.values()]


def get_index_range(index_values: Iterable[int]) -> range:
    """compute the range of index values for a certain dimension."""
    return range(min(index_values) - 1, max(index_values) + 2)


def compute_cycle(grid: Set[Point2D]) -> Set[Point2D]:
    """compute a single cycle of tile switching.

    Copied/stripped down version from day 17.
    """
    new_grid = copy.deepcopy(grid)

    range_x = get_index_range([p.x for p in grid])
    range_y = get_index_range([p.y for p in grid])

    for ix in range_x:
        for iy in range_y:
            current_p = Point2D(ix, iy)

            neighbors = set(get_neighbors(current_p))
            num_black = len(grid.intersection(neighbors))
            if current_p in grid and (num_black == 0 or num_black > 2):
                new_grid.remove(current_p)
            if current_p not in grid and num_black == 2:
                new_grid.add(current_p)

    return new_grid


def main() -> None:
    moves_str = utils.get_input(__file__)

    # part 1
    grid = lay_out_grid(moves_str)
    n_tiles_black = len(grid)
    print(f"part 1: {n_tiles_black}")

    # part 2
    for _ in range(100):
        grid = compute_cycle(grid)
    n_tiles_black_2 = len(grid)
    print(f"part 2: {n_tiles_black_2}")


if __name__ == "__main__":
    main()
