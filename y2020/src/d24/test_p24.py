# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p24 import flip_tile, lay_out_grid, Point2D, compute_cycle


def test_flip_tile():
    grid = set()
    flip_tile("nwwswee", grid)
    assert Point2D(0, 0) in grid


moves_str = """
sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew
"""


def test_lay_out_grid():
    assert len(lay_out_grid(moves_str)) == 10


@pytest.mark.parametrize(
    "n_rounds,expected",
    [
        (1, 15),
        (2, 12),
        (3, 25),
        (4, 14),
        (5, 23),
        (6, 28),
        (7, 41),
        (8, 37),
        (9, 49),
        (10, 37),
        (20, 132),
        (30, 259),
        (40, 406),
        (50, 566),
        (60, 788),
        (70, 1106),
        (80, 1373),
        (90, 1844),
        (100, 2208),
    ],
)
def test_compute_cycle(n_rounds: int, expected: int):
    grid = lay_out_grid(moves_str)
    for _ in range(n_rounds):
        grid = compute_cycle(grid)
    assert len(grid) == expected
