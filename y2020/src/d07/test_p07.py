# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p07 import (
    parse_rules,
    find_containers,
    count_distinct_containers,
    count_bags_within,
)


rules_raw = """
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
"""


def test_count_containers():
    assert count_distinct_containers(parse_rules(rules_raw), "shiny gold bags") == 4


def test_count_bags_within():
    assert count_bags_within(parse_rules(rules_raw), "shiny gold bags") == 32
