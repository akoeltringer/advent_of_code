# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports

# own stuff
from .p12 import run_instructions_ship, run_instructions_waypoint, Point


instructions = [("F", 10), ("N", 3), ("F", 7), ("R", 90), ("F", 11)]


def test_run_instructions_ship():
    assert run_instructions_ship(instructions) == (17, -8)


def test_run_instructions_waypoint():
    assert run_instructions_waypoint(instructions) == Point(214, -72)


def test_point_rotate():
    assert Point(10, 1).rotate(1) == Point(1, -10)
    assert Point(10, 1).rotate(3) == Point(-1, 10)
