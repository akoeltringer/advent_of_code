use std::collections::HashSet;
use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn parse_input(input: &str) -> (HashSet<(usize, usize)>, Vec<(usize, usize)>) {
    let input_split: Vec<&str> = input.trim().split("\n\n").collect();

    let mut points: HashSet<(usize, usize)> = HashSet::new();
    for line in input_split[0].split("\n") {
        let (x_str, y_str) = line.trim().split_once(",").unwrap();
        let x: usize = x_str.trim().parse().expect("error parsing int from string");
        let y: usize = y_str.trim().parse().expect("error parsing int from string");
        points.insert((x, y));
    }

    let mut fold_instr: Vec<(usize, usize)> = Vec::new();
    for line in input_split[1].split("\n") {
        let (dir, val_str) = line.split_once("=").unwrap();
        let val: usize = val_str
            .trim()
            .parse()
            .expect("error parsing int from string");
        match dir {
            "fold along x" => fold_instr.push((val, 0)),
            "fold along y" => fold_instr.push((0, val)),
            _ => panic!("incorrect instruction found"),
        }
    }

    (points, fold_instr)
}

fn fold_point(point: &(usize, usize), by: &(usize, usize)) -> (usize, usize) {
    let mut new_x = point.0;
    let mut new_y = point.1;

    if (by.0 != 0) & (by.0 < point.0) {
        new_x = 2 * by.0 - point.0;
    }

    if (by.1 != 0) & (by.1 < point.1) {
        new_y = 2 * by.1 - point.1;
    }

    (new_x, new_y)
}

fn fold_one(points: &HashSet<(usize, usize)>, by: &(usize, usize)) -> HashSet<(usize, usize)> {
    let mut new_points: HashSet<(usize, usize)> = HashSet::new();

    for point in points {
        new_points.insert(fold_point(point, by));
    }

    new_points
}

fn fold_all(
    points: &HashSet<(usize, usize)>,
    instructions: &Vec<(usize, usize)>,
) -> HashSet<(usize, usize)> {
    let mut result: HashSet<(usize, usize)> = points.clone();

    for instr in instructions {
        result = fold_one(&result, instr);
    }

    result
}

fn print_grid(points: HashSet<(usize, usize)>) {
    let xmax = points.iter().map(|&p| p.0).max().unwrap();
    let ymax = points.iter().map(|&p| p.1).max().unwrap();

    for y in 0..(ymax + 1) {
        let mut out: String = String::new();

        for x in 0..(xmax + 1) {
            if points.contains(&(x, y)) {
                out.push('#');
            } else {
                out.push('.');
            }
        }
        println!("{}", out);
    }
}

fn main() {
    let input = read_file();
    let (points, fold_instr) = parse_input(&input);

    // part 1
    let res = fold_one(&points, &fold_instr[0]);
    println!("Part 1 num elements after first fold: {}", res.len());

    // part 2
    let res = fold_all(&points, &fold_instr);
    println!("Part 2:");
    print_grid(res);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_file() {
        let input = "6,10\n0,14\n9,10\n\nfold along y=7\nfold along x=5";
        let (points, fold_instr) = parse_input(&input);

        assert_eq!(points.len(), 3);
        assert!(points.contains(&(6 as usize, 10 as usize)));
        assert!(points.contains(&(0 as usize, 14 as usize)));
        assert!(points.contains(&(9 as usize, 10 as usize)));

        assert_eq!(fold_instr.len(), 2);
        assert!(fold_instr.contains(&(0 as usize, 7 as usize)));
        assert!(fold_instr.contains(&(5 as usize, 0 as usize)));
    }

    #[test]
    fn test_fold_point_y_folded() {
        let p: (usize, usize) = (6, 12);
        let by: (usize, usize) = (0, 10);
        assert_eq!(fold_point(&p, &by), (6, 8));
    }
    #[test]
    fn test_fold_point_y_not_folded() {
        let p: (usize, usize) = (6, 8);
        let by: (usize, usize) = (0, 10);
        assert_eq!(fold_point(&p, &by), p);
    }
    #[test]
    fn test_fold_point_x_folded() {
        let p: (usize, usize) = (16, 2);
        let by: (usize, usize) = (10, 0);
        assert_eq!(fold_point(&p, &by), (4, 2));
    }
    #[test]
    fn test_fold_point_x_not_folded() {
        let p: (usize, usize) = (9, 8);
        let by: (usize, usize) = (10, 0);
        assert_eq!(fold_point(&p, &by), p);
    }
}
