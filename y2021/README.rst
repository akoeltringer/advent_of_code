Advent of Code 2021
-------------------

This year the language of choice is: `Rust`_ 🥳

To run the tests, enter a "days" directory and run ::

    cargo test

To compile and run the programs, execute ::

    cargo run


.. _Rust: https://www.rust-lang.org/
