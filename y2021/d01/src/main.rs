use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn parse_input(s: &str) -> Vec<i32> {
    let v_str: Vec<&str> = s.trim().split("\n").collect();
    let mut v: Vec<i32> = Vec::new();

    for elem_str in &v_str {
        let elem: i32 = elem_str
            .trim()
            .parse()
            .expect("error parsing the string to int");
        v.push(elem);
    }
    v
}

fn count_increasing(v: &Vec<i32>) -> i32 {
    let mut i = 0;

    let mut prev_elem = &v[0];
    for elem in &v[1..] {
        if elem > prev_elem {
            i += 1;
        }
        prev_elem = elem;
    }
    i
}

fn window_sums(v: &Vec<i32>) -> Vec<i32> {
    let mut res_vec: Vec<i32> = Vec::new();

    let mut elem2 = &v[0];
    let mut elem1 = &v[1];
    for elem in &v[2..] {
        res_vec.push(elem2 + elem1 + elem);
        elem2 = elem1;
        elem1 = elem;
    }

    res_vec
}

fn main() {
    let input = read_file();
    let nums = parse_input(&input);

    // Part 1
    let num_increases = count_increasing(&nums);
    println!(
        "Part 1: Number of measurements larger than the previous one: {}",
        num_increases
    );

    // Part 2
    let num_sum_increases = count_increasing(&window_sums(&nums));
    println!(
        "Part 2: Number of windowed-sum-measurements larger than the previous one: {}",
        num_sum_increases
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parser_general_case() {
        assert_eq!(parse_input("1\n2\n3"), [1, 2, 3]);
    }

    #[test]
    fn parser_single_value() {
        assert_eq!(parse_input("1"), [1]);
    }

    #[test]
    fn count_increasing_general() {
        assert_eq!(count_increasing(&Vec::from([1, 2, 3, 3, 2, 3])), 3);
    }

    #[test]
    fn count_increasing_test_input() {
        let test_input = String::from("199\n200\n208\n210\n200\n207\n240\n269\n260\n263");
        let test_nums = parse_input(&test_input);
        assert_eq!(count_increasing(&test_nums), 7);
    }

    #[test]
    fn window_sums_general() {
        assert_eq!(window_sums(&Vec::from([1, 2, 3, 3, 2, 3])), [6, 8, 8, 8]);
    }

    #[test]
    fn count_increasing_window_sums_test_input() {
        let test_input = String::from("199\n200\n208\n210\n200\n207\n240\n269\n260\n263");
        let test_nums = parse_input(&test_input);
        assert_eq!(count_increasing(&window_sums(&test_nums)), 5);
    }
}
