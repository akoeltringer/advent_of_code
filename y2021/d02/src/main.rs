use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn parse_line(line: &str) -> (&str, i32) {
    let line_parts: Vec<&str> = line.split(" ").collect();
    assert_eq!(line_parts.len(), 2);
    let num: i32 = line_parts[1]
        .trim()
        .parse()
        .expect("error parsing the string to int");

    (line_parts[0], num)
}

fn follow_course(instructions: &Vec<&str>) -> (i32, i32) {
    let mut depth: i32 = 0;
    let mut h_pos: i32 = 0;

    for instr in instructions {
        let (command, num) = parse_line(instr);

        if command == "forward" {
            h_pos += num;
        } else if command == "down" {
            depth += num;
        } else if command == "up" {
            depth -= num;
        }
    }

    (h_pos, depth)
}

fn follow_course_part2(instructions: &Vec<&str>) -> (i32, i32) {
    let mut depth: i32 = 0;
    let mut h_pos: i32 = 0;
    let mut aim: i32 = 0;

    for instr in instructions {
        let (command, num) = parse_line(instr);

        if command == "forward" {
            h_pos += num;
            depth += aim * num;
        } else if command == "down" {
            aim += num;
        } else if command == "up" {
            aim -= num;
        }
    }

    (h_pos, depth)
}

fn main() {
    let input = read_file();
    let input_vec: Vec<&str> = input.trim().split("\n").collect();

    // part 1
    let (h_pos, depth) = follow_course(&input_vec);
    println!("part 1: multiplication yields {}", h_pos * depth);

    // part 2
    let (h_pos, depth) = follow_course_part2(&input_vec);
    println!("part 2: multiplication yields {}", h_pos * depth)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parse_line_general_case() {
        assert_eq!(parse_line("abc 123"), ("abc", 123));
    }

    #[test]
    fn follow_course_test_input() {
        let instr = vec![
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        ];
        assert_eq!(follow_course(&instr), (15, 10))
    }

    #[test]
    fn follow_course_part2_test_input() {
        let instr = vec![
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        ];
        assert_eq!(follow_course_part2(&instr), (15, 60))
    }
}
