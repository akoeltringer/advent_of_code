use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn find_corrupted(line: &str) -> usize {
    let mut stack: Vec<char> = Vec::new();

    for elem in line.chars() {
        match elem {
            '(' | '[' | '{' | '<' => stack.push(elem),
            ')' => {
                if stack.pop() != Some('(') {
                    return 3;
                }
            }
            ']' => {
                if stack.pop() != Some('[') {
                    return 57;
                }
            }
            '}' => {
                if stack.pop() != Some('{') {
                    return 1197;
                }
            }
            '>' => {
                if stack.pop() != Some('<') {
                    return 25137;
                }
            }
            _ => panic!(),
        }
    }

    0
}

fn autocomplete(line: &str) -> usize {
    let mut stack: Vec<char> = Vec::new();

    for elem in line.chars() {
        match elem {
            '(' | '[' | '{' | '<' => stack.push(elem),
            ')' | ']' | '}' | '>' => {
                stack.pop();
            }
            _ => panic!(),
        }
    }

    let mut score: usize = 0;

    loop {
        let next = stack.pop();
        if next == None {
            break;
        }

        score *= 5;
        match next {
            Some('(') => score += 1,
            Some('[') => score += 2,
            Some('{') => score += 3,
            Some('<') => score += 4,
            _ => panic!(),
        }
    }

    score
}

fn autocomplete_score(input: &str) -> usize {
    let mut autocomp_scores: Vec<usize> = input
        .trim()
        .split("\n")
        .filter(|&x| find_corrupted(x) == 0)
        .map(|x| autocomplete(x))
        .collect();
    autocomp_scores.sort_unstable();
    // to get the middle element, should be (len+1) / 2, but indexes are
    // 0-based, so using -1 instead
    autocomp_scores[(autocomp_scores.len() - 1) / 2]
}

fn main() {
    let input = read_file();

    // part 1
    let error_score: usize = input.trim().split("\n").map(|x| find_corrupted(x)).sum();
    println!("part 1 error score: {}", error_score);

    // part 2
    let median: usize = autocomplete_score(&input);
    println!("part 2 autocomplete score: {}", median);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_corrupted_1() {
        assert_eq!(find_corrupted("[({(<(())[]>[[{[]{<()<>>"), 0);
    }
    #[test]
    fn find_corrupted_2() {
        assert_eq!(find_corrupted("{([(<{}[<>[]}>{[]{[(<()>"), 1197);
    }
    #[test]
    fn find_corrupted_3() {
        assert_eq!(find_corrupted("[[<[([]))<([[{}[[()]]]"), 3);
    }
    #[test]
    fn find_corrupted_4() {
        assert_eq!(find_corrupted("[{[{({}]{}}([{[{{{}}([]"), 57);
    }
    #[test]
    fn find_corrupted_5() {
        assert_eq!(find_corrupted("<{([([[(<>()){}]>(<<{{"), 25137);
    }

    #[test]
    fn test_autocomplete_1() {
        assert_eq!(autocomplete("[({(<(())[]>[[{[]{<()<>>"), 288957)
    }
    #[test]
    fn test_autocomplete_2() {
        assert_eq!(autocomplete("[(()[<>])]({[<{<<[]>>("), 5566)
    }
    #[test]
    fn test_autocomplete_3() {
        assert_eq!(autocomplete("(((({<>}<{<{<>}{[]{[]{}"), 1480781)
    }
    #[test]
    fn test_autocomplete_4() {
        assert_eq!(autocomplete("{<[[]]>}<{[{[{[]{()[[[]"), 995444)
    }
    #[test]
    fn test_autocomplete_5() {
        assert_eq!(autocomplete("<{([{{}}[<[[[<>{}]]]>[]]"), 294)
    }
    #[test]
    fn test_autocomp_score() {
        assert_eq!(
            autocomplete_score(
                "[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]"
            ),
            288957
        );
    }
}
