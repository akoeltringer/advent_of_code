use std::collections::{HashMap, HashSet};
use std::fs;

struct CaveMap {
    map: HashMap<String, HashSet<String>>,
}

impl CaveMap {
    fn new() -> CaveMap {
        let map: HashMap<String, HashSet<String>> = HashMap::new();
        CaveMap { map }
    }

    fn from_file() -> CaveMap {
        let input = fs::read_to_string("input.txt").expect("Something went wrong reading the file");
        CaveMap::from_string(input)
    }

    fn from_string(input: String) -> CaveMap {
        let mut cave_map = CaveMap::new();

        for line in input.trim().split("\n") {
            let (left, right) = line.trim().split_once("-").unwrap();
            // insert left -> right
            cave_map.add_segment(left.to_string(), right.to_string());
            // insert right -> left
            cave_map.add_segment(right.to_string(), left.to_string())
        }

        cave_map
    }

    fn add_segment(&mut self, left: String, right: String) {
        // start is only added on left, end is only added on right
        if (right == "start") | (left == "end") {
            return;
        }
        let entry = self.map.entry(left).or_insert(HashSet::new());
        entry.insert(right);
    }

    /// find all possible paths
    /// the `small_caves_visited_twice` keeps track if a small cave has already been
    /// visited twice. for the first part, set to `true` from the beginning...
    fn find_paths<'a>(
        &'a self,
        paths: &mut Vec<Vec<&'a str>>,
        current_path: Vec<&'a str>,
        small_cave_visited_twice: bool,
    ) {
        let loc = match current_path.last() {
            Some(&"end") => {
                paths.push(current_path);
                return;
            }
            Some(x) => x,
            None => "start",
        };

        for elem in self.map.get(loc).unwrap() {
            // need to reset this here for every element in this iteration
            let mut small_cave_visited_twice_new = small_cave_visited_twice;

            if (current_path.contains(&&elem[..])) & (*elem == elem.to_ascii_lowercase()) {
                if small_cave_visited_twice_new {
                    // small cave was already visited
                    continue;
                }
                small_cave_visited_twice_new = true;
            }
            let mut new_path = current_path.clone();
            new_path.push(elem);
            self.find_paths(paths, new_path, small_cave_visited_twice_new);
        }
    }
}

fn main() {
    let cm = CaveMap::from_file();

    // part 1
    let mut paths: Vec<Vec<&str>> = Vec::new();
    cm.find_paths(&mut paths, Vec::new(), true);
    println!("part 1 number of paths: {}", paths.len());

    // part 2
    let mut paths: Vec<Vec<&str>> = Vec::new();
    cm.find_paths(&mut paths, Vec::new(), false);
    println!("part 2 number of paths: {}", paths.len())
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> String {
        "start-A\n\
         start-b\n\
         A-c\n\
         A-b\n\
         b-d\n\
         A-end\n\
         b-end"
            .to_string()
    }

    fn get_test_input_2() -> String {
        "dc-end\n\
         HN-start\n\
         start-kj\n\
         dc-start\n\
         dc-HN\n\
         LN-dc\n\
         HN-end\n\
         kj-sa\n\
         kj-HN\n\
         kj-dc"
            .to_string()
    }

    fn get_test_input_3() -> String {
        "fs-end
         he-DX
         fs-he
         start-DX
         pj-DX
         end-zg
         zg-sl
         zg-pj
         pj-he
         RW-he
         fs-DX
         pj-RW
         zg-RW
         start-pj
         he-WI
         zg-he
         pj-fs
         start-RW"
            .to_string()
    }

    #[test]
    fn test_cavemap_from_string() {
        let cm = CaveMap::from_string(get_test_input());
        assert_eq!(cm.map.len(), 5);
        assert_eq!(cm.map.get("start").unwrap().len(), 2);
        assert_eq!(cm.map.get("b").unwrap().len(), 3);
        assert_eq!(cm.map.get("A").unwrap().len(), 3);
    }

    #[test]
    fn test_cavemap_find_paths_part1_ex1() {
        let cm = CaveMap::from_string(get_test_input());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), true);
        assert_eq!(paths.len(), 10);
    }

    #[test]
    fn test_cavemap_find_paths_part1_ex2() {
        let cm = CaveMap::from_string(get_test_input_2());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), true);
        assert_eq!(paths.len(), 19);
    }

    #[test]
    fn test_cavemap_find_paths_part1_ex3() {
        let cm = CaveMap::from_string(get_test_input_3());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), true);
        assert_eq!(paths.len(), 226);
    }

    #[test]
    fn test_cavemap_find_paths_part2_ex1() {
        let cm = CaveMap::from_string(get_test_input());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), false);
        assert_eq!(paths.len(), 36);
    }

    #[test]
    fn test_cavemap_find_paths_part2_ex2() {
        let cm = CaveMap::from_string(get_test_input_2());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), false);
        assert_eq!(paths.len(), 103);
    }

    #[test]
    fn test_cavemap_find_paths_part2_ex3() {
        let cm = CaveMap::from_string(get_test_input_3());
        let mut paths: Vec<Vec<&str>> = Vec::new();

        cm.find_paths(&mut paths, Vec::new(), false);
        assert_eq!(paths.len(), 3509);
    }
}
