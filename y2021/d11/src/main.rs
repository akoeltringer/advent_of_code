use std::collections::HashSet;
use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

/// Convert the input to a grid with borders
fn to_grid(input: &str) -> Vec<Vec<usize>> {
    let mut grid: Vec<Vec<usize>> = Vec::new();

    for line in input.trim().split("\n") {
        let line_vec: Vec<usize> = line
            .chars()
            .map(|x| {
                x.to_string()
                    .parse()
                    .expect("failed converting char to number")
            })
            .collect();

        grid.push(line_vec);
    }

    if (grid.len() != 10) | (grid[0].len() != 10) {
        panic!("not a 10x10 grid")
    }

    grid
}

fn flash(grid: &mut Vec<Vec<usize>>, i: usize, j: usize, flashed: &mut HashSet<(usize, usize)>) {
    if (grid[i][j] <= 9) | (flashed.contains(&(i, j))) {
        return;
    }
    flashed.insert((i, j));

    for i2 in (i as isize - 1)..(i as isize + 2) {
        if (i2 < 0) | (i2 > 9) {
            continue;
        }
        let i2 = i2 as usize;

        for j2 in (j as isize - 1)..(j as isize + 2) {
            if (j2 < 0) | (j2 > 9) {
                continue;
            }
            let j2 = j2 as usize;

            grid[i2][j2] += 1;
            flash(grid, i2, j2, flashed);
        }
    }
}

fn step(grid: &mut Vec<Vec<usize>>) -> usize {
    let mut flashed: HashSet<(usize, usize)> = HashSet::new();

    // step1+2: increase energy levels and flash
    for i in 0..10 {
        for j in 0..10 {
            grid[i][j] += 1;
            flash(grid, i, j, &mut flashed);
        }
    }

    // step3: set flashed to zero
    for (i, j) in &flashed {
        grid[*i][*j] = 0;
    }

    // return the number of flashed cells
    flashed.len()
}

fn part1(grid: &mut Vec<Vec<usize>>) -> usize {
    let mut n_flashes: usize = 0;
    for _ in 0..100 {
        n_flashes += step(grid);
    }
    n_flashes
}

fn part2(grid: &mut Vec<Vec<usize>>) -> usize {
    let mut n_step: usize = 0;
    loop {
        n_step += 1;
        let n_flashes: usize = step(grid);
        if n_flashes == 100 {
            return n_step;
        }
    }
}

fn main() {
    let input = read_file();

    // part 1
    let mut grid = to_grid(&input);
    let n_flashes: usize = part1(&mut grid);
    println!("part 1 number of flashes in 100 steps: {}", n_flashes);

    // part 2
    let mut grid = to_grid(&input);
    let n_step: usize = part2(&mut grid);
    println!("part 2 number of steps for first all-flash: {}", n_step);
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_grid() -> Vec<Vec<usize>> {
        to_grid(
            "5483143223\n\
             2745854711\n\
             5264556173\n\
             6141336146\n\
             6357385478\n\
             4167524645\n\
             2176841721\n\
             6882881134\n\
             4846848554\n\
             5283751526",
        )
    }

    #[test]
    fn test_step_1() {
        let mut grid = get_test_grid();
        step(&mut grid);

        assert_eq!(
            grid,
            to_grid(
                "6594254334\n\
                 3856965822\n\
                 6375667284\n\
                 7252447257\n\
                 7468496589\n\
                 5278635756\n\
                 3287952832\n\
                 7993992245\n\
                 5957959665\n\
                 6394862637"
            )
        );
    }

    #[test]
    fn test_step_2() {
        let mut grid = get_test_grid();
        step(&mut grid);
        assert_eq!(step(&mut grid), 35);

        assert_eq!(
            grid,
            to_grid(
                "8807476555\n\
                 5089087054\n\
                 8597889608\n\
                 8485769600\n\
                 8700908800\n\
                 6600088989\n\
                 6800005943\n\
                 0000007456\n\
                 9000000876\n\
                 8700006848"
            )
        );
    }

    #[test]
    fn test_step_3() {
        let mut grid = get_test_grid();
        step(&mut grid);
        step(&mut grid);
        assert_eq!(step(&mut grid), 45);

        assert_eq!(
            grid,
            to_grid(
                "0050900866\n\
                 8500800575\n\
                 9900000039\n\
                 9700000041\n\
                 9935080063\n\
                 7712300000\n\
                 7911250009\n\
                 2211130000\n\
                 0421125000\n\
                 0021119000"
            )
        );
    }

    #[test]
    fn test_part1() {
        let mut grid = get_test_grid();
        let n_flashes = part1(&mut grid);

        assert_eq!(
            grid,
            to_grid(
                "0397666866\n\
                 0749766918\n\
                 0053976933\n\
                 0004297822\n\
                 0004229892\n\
                 0053222877\n\
                 0532222966\n\
                 9322228966\n\
                 7922286866\n\
                 6789998766"
            )
        );
        assert_eq!(n_flashes, 1656)
    }

    #[test]
    fn test_part2() {
        let mut grid = get_test_grid();
        assert_eq!(part2(&mut grid), 195)
    }
}
