use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn gamma_epsilon(vec: &Vec<&str>) -> (u32, u32) {
    let mut gamma: u32 = 0;
    let mut epsilon: u32 = 0;
    let mut ones_count_vec: Vec<usize> = vec![0; vec[0].chars().count()];

    let half = vec.len() / 2;

    for line in vec {
        for (i, val) in line.chars().enumerate() {
            if val == '1' {
                ones_count_vec[i] += 1;
            }
        }
    }

    for (i, cnt) in ones_count_vec.iter().enumerate() {
        if cnt < &half {
            gamma += 2u32.pow((ones_count_vec.len() - i - 1) as u32);
        } else {
            epsilon += 2u32.pow((ones_count_vec.len() - i - 1) as u32);
        };
    }

    (epsilon, gamma)
}

fn oxygen_generator_rating(vec: &Vec<&str>, idx: usize) -> usize {
    if vec.len() == 1 {
        return usize::from_str_radix(&vec[0], 2).unwrap();
    }

    let mut ones: Vec<&str> = Vec::new();
    let mut zeros: Vec<&str> = Vec::new();

    for elem in vec {
        if elem.as_bytes()[idx] == 49 {
            // ascii 49 = '1'
            ones.push(elem);
        } else {
            zeros.push(elem);
        }
    }
    if ones.len() >= zeros.len() {
        oxygen_generator_rating(&ones, idx + 1)
    } else {
        oxygen_generator_rating(&zeros, idx + 1)
    }
}

fn co2_scrubber_rating(vec: &Vec<&str>, idx: usize) -> usize {
    if vec.len() == 1 {
        return usize::from_str_radix(&vec[0], 2).unwrap();
    }

    let mut ones: Vec<&str> = Vec::new();
    let mut zeros: Vec<&str> = Vec::new();

    for elem in vec {
        if elem.as_bytes()[idx] == 49 {
            // ascii 49 = '1'
            ones.push(elem);
        } else {
            zeros.push(elem);
        }
    }
    if ones.len() >= zeros.len() {
        co2_scrubber_rating(&zeros, idx + 1)
    } else {
        co2_scrubber_rating(&ones, idx + 1)
    }
}

fn main() {
    let input = read_file();
    let input_vec: Vec<&str> = input.trim().split("\n").collect();

    // part 1
    let (g, e) = gamma_epsilon(&input_vec);
    println!("power consumption is {}", g * e);

    // part 2
    let o2gen = oxygen_generator_rating(&input_vec, 0);
    let co2sc = co2_scrubber_rating(&input_vec, 0);
    println!("life supporting rate: {}", o2gen * co2sc);
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_input() -> Vec<&'static str> {
        vec![
            "00100", "11110", "10110", "10111", "10101", "01111", "00111", "11100", "10000",
            "11001", "00010", "01010",
        ]
    }

    #[test]
    fn test_gamma_epsilon() {
        assert_eq!(gamma_epsilon(&get_input()), (22, 9));
    }

    #[test]
    fn test_oxygen_generator_rating() {
        assert_eq!(oxygen_generator_rating(&get_input(), 0), 23)
    }

    #[test]
    fn test_co2_scrubber_rating() {
        assert_eq!(co2_scrubber_rating(&get_input(), 0), 10)
    }
}
