use clap::{App, Arg};
use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn iterate(mut v: Vec<usize>) -> Vec<usize> {
    let mut append = 0;
    for num in &mut v {
        if *num == 0 {
            *num = 6;
            append += 1;
        } else {
            *num -= 1;
        }
    }
    for _ in 0..append {
        v.push(8);
    }
    v
}

fn count_occurrences(input: &Vec<usize>) -> [usize; 9] {
    let mut occ = [0; 9];
    for i in 0..9 {
        occ[i] = input.iter().filter(|&x| *x == i).count();
    }
    occ
}

fn iterate2(n_iter: usize, fish: [usize; 9]) -> usize {
    if n_iter == 0 {
        return fish.iter().sum();
    }

    iterate2(
        n_iter - 1,
        [
            fish[1],
            fish[2],
            fish[3],
            fish[4],
            fish[5],
            fish[6],
            fish[7] + fish[0],
            fish[8],
            fish[0],
        ],
    )
}

fn main() {
    let matches = App::new("Advent Of Code 2021 - day 6")
        .version("0.1.0")
        .author("akoeltringer")
        .arg(Arg::with_name("debug").short("d").long("debug"))
        .get_matches();

    if matches.is_present("debug") {
        // print out evolution to find patterns
        // - there are none, need different representation of problem...
        let mut v = vec![3, 4, 3, 1, 2];
        for i in 0..30 {
            println!("Step {}: {:?}", i, v);
            v = iterate(v);
        }
    } else {
        let input = read_file();
        let initial_state: Vec<usize> = input
            .trim()
            .split(",")
            .map(|x| x.parse::<usize>().unwrap())
            .collect();

        // Part 1
        let mut v = initial_state.clone();
        for _ in 0..80 {
            v = iterate(v);
        }
        println!("Part 1: number of fish after 80 iterations: {}", v.len());

        // Part 2
        let res = iterate2(256, count_occurrences(&initial_state));
        println!("Part 2: number of fish after 256 iterations: {}", res);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_iterate_1() {
        assert_eq!(iterate(vec![3, 4, 3, 1, 2]), [2, 3, 2, 0, 1]);
    }

    #[test]
    fn test_iterate_2() {
        assert_eq!(iterate(vec![2, 3, 2, 0, 1]), [1, 2, 1, 6, 0, 8]);
    }

    #[test]
    fn test_iterate_len_1() {
        let mut v = vec![3, 4, 3, 1, 2];
        for _ in 0..18 {
            v = iterate(v);
        }
        assert_eq!(v.len(), 26);
    }

    #[test]
    fn test_iterate_len_2() {
        let mut v = vec![3, 4, 3, 1, 2];
        for _ in 0..80 {
            v = iterate(v);
        }
        assert_eq!(v.len(), 5934);
    }

    #[test]
    fn test_count_occurrences() {
        assert_eq!(
            count_occurrences(&vec![3, 4, 3, 1, 2]),
            [0, 1, 1, 2, 1, 0, 0, 0, 0]
        );
    }

    #[test]
    fn test_iterate_len_3() {
        assert_eq!(iterate2(256, [0, 1, 1, 2, 1, 0, 0, 0, 0]), 26984457539);
    }
}
