use std::cmp;

fn find_x_velocity(x_min: i32, x_max: i32) -> i32 {
    // finding x in the first part: the velocity will be 0
    // in the end, so increase numbers until one ends up between
    // the bounds
    let mut v_x: i32 = 0;
    loop {
        v_x += 1;

        let x_end: i32 = v_x * (v_x + 1) / 2;
        if x_end >= x_max {
            panic!("x overshoot")
        }
        if x_end >= x_min {
            break;
        }
    }
    v_x
}

fn compute_trajectory(
    x_min: i32,
    x_max: i32,
    y_min: i32,
    y_max: i32,
    x_vel: i32,
    y_vel: i32,
) -> (bool, i32) {
    let mut x: i32 = 0;
    let mut y: i32 = 0;
    let mut x_vel = x_vel;
    let mut y_vel = y_vel;
    let mut y_peak: i32 = 0;

    loop {
        x += x_vel;
        y += y_vel;
        if y > y_peak {
            y_peak = y;
        }

        if (x > x_max) | (y < y_min) {
            return (false, 0);
        }

        if (x >= x_min) & (y <= y_max) {
            return (true, y_peak);
        }

        x_vel = cmp::max(x_vel - 1, 0);
        y_vel -= 1;
    }
}

fn find_y_peak(x_min: i32, x_max: i32, y_min: i32, y_max: i32) -> (i32, i32, i32) {
    let mut y_vel_start = 200;
    let x_vel_start = find_x_velocity(x_min, x_max);

    loop {
        let (success, y_peak) =
            compute_trajectory(x_min, x_max, y_min, y_max, x_vel_start, y_vel_start);

        if success {
            return (y_peak, x_vel_start, y_vel_start);
        }
        y_vel_start -= 1;

        if y_vel_start == 0 {
            panic!("found negative start velocity for y")
        }
    }
}

fn brute_force_grid_search(
    x_min: i32,
    x_max: i32,
    y_min: i32,
    y_max: i32,
    x_vel_min: i32,
    y_vel_max: i32,
) -> i32 {
    // for x_vel
    // - x_vel_start (found in part 1) is the lower bound
    // - x_max is the upper bound ("do it in 1 step")
    //
    // for y_vel
    // - y_vel_start (found in part 1) is the upper bound
    // - y_min is the lower bound ("do it in 1 step")

    let mut n_success = 0;

    for x_vel_start in x_vel_min..(x_max + 1) {
        for y_vel_start in (y_min)..(y_vel_max + 1) {
            let (success, _) =
                compute_trajectory(x_min, x_max, y_min, y_max, x_vel_start, y_vel_start);

            if success {
                n_success += 1;
            }
        }
    }
    n_success
}

fn main() {
    let x_min: i32 = 128;
    let x_max: i32 = 160;
    let y_min: i32 = -142;
    let y_max: i32 = -88;

    // part 1
    let (y_peak, x_vel_start, y_vel_start) = find_y_peak(x_min, x_max, y_min, y_max);
    println!(
        "part 1 y peak: {} (start velocity was ({}, {}))",
        y_peak, x_vel_start, y_vel_start
    );

    // part 2
    let n_success = brute_force_grid_search(x_min, x_max, y_min, y_max, x_vel_start, y_vel_start);
    println!("part 2 total successes: {}", n_success);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_x_velocity() {
        assert_eq!(find_x_velocity(20, 30), 6);
    }

    #[test]
    fn test_find_y_peak() {
        assert_eq!(find_y_peak(20, 30, -10, -5), (45, 6, 9));
    }

    #[test]
    fn test_brute_force_grid_search() {
        assert_eq!(brute_force_grid_search(20, 30, -10, -5, 6, 9), 112);
    }
}
