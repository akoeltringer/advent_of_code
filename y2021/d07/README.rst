Day 7
-----

... is basically statistics, so use some lines of Python :-)

Part 1
^^^^^^

For the ideal position: this is an optimization problem:
::
    min Σ | x_i - i |

... which corresponds to a "V" shaped function - the Median solves
this optimization problem.

So: ::

    import pandas

    data = pandas.Series([...])
    pos = data.median()
    fuel = (data - pos).abs().sum()
    print("part1: fuel used: %s", fuel)


Part 2
^^^^^^

As the fuel expenditure increases quadratically with the distance, this
time the optimization function is ::

    min Σ ( x_i - i ) ^ 2

... which is solved by the mean. Fuel expenditure is a cumulative sum, which
can be computed with Gauss sum formula n * (n+1) / 2. So, ::

    def g(n):
        return n * (n+1) / 2

    pos2 = data.mean().round()
    fuel2 = (data - pos2).abs().apply(g).sum()
    print("part2: fuel used: %s", fuel2)


Note: the mean does usually not give an "integer" - which I solved with rounding. Still
it was "off by one" which I found out by checking solutions nearby, i.e. +/- 1, 2, 3
