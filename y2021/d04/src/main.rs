use std::fs;

struct Board {
    nums: Vec<usize>,
    matched: Vec<usize>, // initialized with 1, set to 0 when "matched"
    nrows: usize,
    ncols: usize,
}

impl Board {
    fn from_string(input: &str) -> Board {
        let nums: Vec<usize> = input
            .split_ascii_whitespace()
            .map(|x| x.parse::<usize>().unwrap())
            .collect();
        let matched: Vec<usize> = vec![1; nums.len()];
        let nrows = input.matches("\n").count() + 1;
        let ncols = nums.len() / nrows;

        Board {
            nums,
            matched,
            nrows,
            ncols,
        }
    }

    fn col_matched(&self, ncol: usize) -> bool {
        let mut s = 0;
        for i in (ncol..(self.ncols * self.nrows + ncol)).step_by(self.ncols) {
            s += self.matched[i];
        }
        s == 0
    }

    fn row_matched(&self, nrow: usize) -> bool {
        self.matched[(nrow * self.ncols)..((nrow + 1) * self.ncols)]
            .iter()
            .sum::<usize>()
            == 0
    }

    fn won(&self) -> bool {
        for i in 0..self.ncols {
            if self.col_matched(i) {
                return true;
            }
        }
        for i in 0..self.nrows {
            if self.row_matched(i) {
                return true;
            }
        }
        false
    }

    fn play(&mut self, num: &usize) {
        let index = self.nums.iter().position(|&r| &r == num);
        match index {
            Some(x) => self.matched[x] = 0,
            None => {}
        }
    }

    fn score(&self) -> Option<usize> {
        if !self.won() {
            None
        } else {
            Some(
                self.nums
                    .iter()
                    .zip(self.matched.iter())
                    .map(|(x1, x2)| x1 * x2)
                    .sum(),
            )
        }
    }
}

struct Game {
    num_seq: Vec<usize>,
    boards: Vec<Board>,
    n_boards: usize,
}

impl Game {
    fn from_string(input_str: &str) -> Game {
        let input_vec: Vec<&str> = input_str.trim().split("\n\n").collect();

        let num_seq = input_vec[0]
            .split(",")
            .map(|x| x.parse::<usize>().unwrap())
            .collect();

        let mut boards: Vec<Board> = Vec::new();
        for board_str in &input_vec[1..] {
            boards.push(Board::from_string(board_str));
        }
        let n_boards = boards.len();

        Game {
            num_seq,
            boards,
            n_boards,
        }
    }

    fn play(&mut self) -> usize {
        for num in &self.num_seq {
            for board in &mut self.boards {
                board.play(num);
                if board.won() {
                    return num * board.score().unwrap();
                }
            }
        }
        panic!()
    }

    fn play_until_last(&mut self) -> usize {
        let mut num_boards_won = 0;

        for num in &self.num_seq {
            for board in &mut self.boards {
                if board.won() {
                    continue;
                }

                board.play(num);
                if board.won() {
                    num_boards_won += 1;
                    if num_boards_won == self.n_boards {
                        return num * board.score().unwrap();
                    }
                }
            }
        }
        panic!()
    }
}

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn main() {
    let input_str = read_file();

    // part 1
    let mut game = Game::from_string(&input_str);
    let result = game.play();
    println!("part 1 score: {}", result);

    // part 2
    let mut game2 = Game::from_string(&input_str);
    let result2 = game2.play_until_last();
    println!("part 2 score: {}", result2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_board_from_str() {
        let input =
            "22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19";
        let board = Board::from_string(&input);

        assert_eq!(board.nrows, 5);
        assert_eq!(board.ncols, 5);
        assert_eq!(
            board.nums,
            vec![
                22, 13, 17, 11, 0, 8, 2, 23, 4, 24, 21, 9, 14, 16, 7, 6, 10, 3, 18, 5, 1, 12, 20,
                15, 19
            ]
        );
    }

    #[test]
    fn test_board_col_matched2() {
        let board = Board {
            nums: Vec::new(),
            matched: vec![0, 0, 0, 1],
            nrows: 2,
            ncols: 2,
        };
        assert_eq!(board.col_matched(0), true);
        assert_eq!(board.col_matched(1), false);
    }

    #[test]
    fn test_board_col_matched3() {
        let board = Board {
            nums: Vec::new(),
            matched: vec![1, 0, 0, 0, 0, 0, 0, 0, 1],
            nrows: 3,
            ncols: 3,
        };
        assert_eq!(board.col_matched(0), false);
        assert_eq!(board.col_matched(1), true);
        assert_eq!(board.col_matched(2), false);
    }

    #[test]
    fn test_board_row_matched2() {
        let board = Board {
            nums: Vec::new(),
            matched: vec![0, 0, 0, 1],
            nrows: 2,
            ncols: 2,
        };
        assert_eq!(board.row_matched(0), true);
        assert_eq!(board.row_matched(1), false);
    }

    #[test]
    fn test_board_row_matched3() {
        let board = Board {
            nums: Vec::new(),
            matched: vec![0, 0, 0, 1, 0, 0, 0, 0, 1],
            nrows: 3,
            ncols: 3,
        };
        assert_eq!(board.row_matched(0), true);
        assert_eq!(board.row_matched(1), false);
        assert_eq!(board.row_matched(1), false);
    }

    #[test]
    fn test_play() {
        let mut board = Board::from_string(
            "22 13 17 11  0\n 8  2 23  4 24\n21  9 14 16  7\n 6 10  3 18  5\n 1 12 20 15 19",
        );
        board.play(&23);
        assert_eq!(board.matched[7], 0);
        assert_eq!(board.matched.iter().sum::<usize>(), 24);
    }

    #[test]
    fn test_score() {
        let board = Board {
            nums: vec![9, 8, 7, 6, 5, 4, 3, 2, 1],
            matched: vec![0, 0, 0, 1, 1, 1, 1, 1, 1],
            nrows: 3,
            ncols: 3,
        };
        assert_eq!(board.score().unwrap(), 21);
    }

    fn get_example_game() -> Game {
        let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";
        Game::from_string(&input)
    }

    #[test]
    fn test_game_from_str() {
        let game = get_example_game();
        assert_eq!(
            game.num_seq,
            vec![
                7, 4, 9, 5, 11, 17, 23, 2, 0, 14, 21, 24, 10, 16, 13, 6, 15, 25, 12, 22, 18, 20, 8,
                19, 3, 26, 1
            ]
        );
        assert_eq!(game.boards.len(), 3);
    }

    #[test]
    fn test_game_play() {
        let mut game = get_example_game();
        let result = game.play();
        assert_eq!(result, 4512)
    }

    #[test]
    fn test_game_play_until_last() {
        let mut game = get_example_game();
        let result = game.play_until_last();
        assert_eq!(result, 1924)
    }
}
