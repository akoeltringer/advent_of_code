/*
Thoughts
--------

Patterns of digits:
    0 abcefg
    1 cf
    2 acdeg
    3 acdfg
    4 bcdf
    5 abdfg
    6 abdefg
    7 acf
    8 abcdefg
    9 abcdfg

How often do segments appear:
    'a' = 8
    'b' = 6
    'c' = 8
    'd' = 7
    'e' = 7
    'f' = 9
    'g' = 7

How to determine segments:
    'b': count = 6
    'f': count = 9
    'c': '1': len = 2
    'a': '7': len = 3
    'd': '4': len = 4

At this point, 'e' and 'g' are missing. '2', '3' and '5' have all length 5,
but only '3' and '5' have 1 unknown (i.e. 'g'). So search through all digits
with len=5 to find out g:
    'g': '3' or '5': len = 5

The last missing letter is 'e'.

 */

use std::collections::HashMap;
use std::fs;

fn get_digits_mapping() -> HashMap<&'static str, char> {
    let m: HashMap<&str, char> = vec![
        ("abcefg", '0'),
        ("cf", '1'),
        ("acdeg", '2'),
        ("acdfg", '3'),
        ("bcdf", '4'),
        ("abdfg", '5'),
        ("abdefg", '6'),
        ("acf", '7'),
        ("abcdefg", '8'),
        ("abcdfg", '9'),
    ]
    .into_iter()
    .collect();
    m
}

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn count_easy(lines: &str) -> usize {
    let mut total: usize = 0;

    for line in lines.trim().split("\n") {
        let parts: Vec<&str> = line.split(" | ").collect();
        if parts.len() != 2 {
            panic!()
        }
        // 1: len=2, 4: len=4, 7: len=3, 8: len=7
        total += parts[1]
            .split(" ")
            .map(|x| x.chars().count())
            .filter(|x| [2, 3, 4, 7].contains(x))
            .count()
    }

    total
}

fn find_by_value(input: &HashMap<char, usize>, val: usize) -> Option<char> {
    input
        .iter()
        .find_map(|(&k, &v)| if v == val { Some(k) } else { None })
}

fn find_by_len(digits: &Vec<&str>, len: usize, known: &HashMap<char, char>) -> Option<char> {
    let elem: &str = digits.iter().filter(|&&x| x.len() == len).nth(0).unwrap();
    for c in elem.chars() {
        if !known.contains_key(&c) {
            return Some(c);
        }
    }
    None
}

fn find_g(digits: &Vec<&str>, known: &HashMap<char, char>) -> Option<char> {
    let candidates: Vec<&str> = digits
        .iter()
        .filter(|&&x| x.len() == 5)
        .map(|&x| x)
        .collect();

    for elem in candidates {
        let mut num_unknowns: usize = 0;
        let mut last_unknown: char = 'z';

        for c in elem.chars() {
            if !known.contains_key(&c) {
                num_unknowns += 1;
                last_unknown = c;
            }
        }
        if last_unknown == 'z' {
            panic!("error when trying to find out 'g'")
        }

        if num_unknowns == 1 {
            return Some(last_unknown);
        }
    }
    None
}

fn find_e(known: &HashMap<char, char>) -> Option<char> {
    for c in "abcdefg".chars() {
        if !known.contains_key(&c) {
            return Some(c);
        }
    }
    None
}

fn solve_mapping(puzzle: &str) -> HashMap<char, char> {
    // count letters on left side (input)
    let mut letter_count: HashMap<char, usize> = HashMap::new();
    for elem in "abcdefg".chars() {
        let cnt: usize = puzzle.chars().filter(|&x| x == elem).count();
        letter_count.insert(elem, cnt);
    }
    // split up the left part into digits
    let digits: Vec<&str> = puzzle.split(" ").collect();

    // the map where to store the codings
    let mut map: HashMap<char, char> = HashMap::new();

    // 'b', 'f'
    map.insert(find_by_value(&letter_count, 6).unwrap(), 'b');
    map.insert(find_by_value(&letter_count, 9).unwrap(), 'f');

    // 'c': find the '1' which is 'cf' and 'f' is already known
    map.insert(find_by_len(&digits, 2, &map).unwrap(), 'c');

    // 'a': find the '7' which is 'acf' and 'c' and 'f' are already known
    map.insert(find_by_len(&digits, 3, &map).unwrap(), 'a');

    // 'd': find the '4' which is 'bcdf' and 'b', 'c' and 'f' are already known
    map.insert(find_by_len(&digits, 4, &map).unwrap(), 'd');

    // 'g'
    map.insert(find_g(&digits, &map).unwrap(), 'g');

    // 'e'
    map.insert(find_e(&map).unwrap(), 'e');

    map
}

fn map2digits(decoded: &str) -> usize {
    let mut ordered = String::new();

    for elem in decoded.split(" ") {
        let mut elem_parts: Vec<char> = elem.chars().collect();
        elem_parts.sort();
        for e in elem_parts {
            ordered.push(e);
        }
        ordered.push(' ');
    }

    let seg2num = get_digits_mapping();
    let mut result = String::new();
    for digit in ordered.trim().split(" ") {
        result.push(*seg2num.get(digit).unwrap());
    }

    result
        .trim()
        .parse::<usize>()
        .expect("error parsing number")
}

fn decode_line(line: &str) -> usize {
    let parts: Vec<&str> = line.split(" | ").collect();
    if parts.len() != 2 {
        panic!()
    }

    // get the mapping and apply it to the input
    let map = solve_mapping(parts[0]);
    let mut decoded = String::new();

    for c in parts[1].chars() {
        decoded.push(match map.get(&c) {
            Some(x) => *x,
            None => ' ',
        });
    }

    // get the (decimal) numbers from the "7-segment" values
    map2digits(&decoded)
}

fn count_hard(lines: &str) -> usize {
    let mut total: usize = 0;

    for line in lines.trim().split("\n") {
        total += decode_line(line);
    }

    total
}

fn main() {
    let input = read_file();

    // part 1
    println!("Part 1 easy count is {}", count_easy(&input));

    // part 2
    println!("Part 2 hard count is {}", count_hard(&input));
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_input() -> &'static str {
        "be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce"
    }

    #[test]
    fn test_count_easy() {
        let input = get_test_input();
        assert_eq!(count_easy(input), 26);
    }

    #[test]
    fn test_solve_mapping() {
        // result: a: d, b: e, c: a, d: f, e: g, f: b, g: c
        let test_str = "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab";
        let map = solve_mapping(test_str);
        for (k, v) in &[
            ('a', 'd'),
            ('b', 'e'),
            ('c', 'a'),
            ('d', 'f'),
            ('e', 'g'),
            ('f', 'b'),
            ('g', 'c'),
        ] {
            // the list above contains the encode mapping, but the solve mapping function
            // returns the decode mapping, that's why below, 'v' is used in get(v)
            assert_eq!(map.get(v).unwrap(), k);
        }
    }

    #[test]
    fn test_decode_line() {
        let test_str =
            "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
        assert_eq!(decode_line(test_str), 5353);
    }

    #[test]
    fn test_count_hard() {
        let input = get_test_input();
        assert_eq!(count_hard(input), 61229);
    }
}
