use std::collections::HashMap;
use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn parse_input(input: &str) -> (&str, HashMap<&str, char>) {
    let (template, replace_str) = input.trim().split_once("\n\n").unwrap();

    let mut replace_rules: HashMap<&str, char> = HashMap::new();
    for line in replace_str.split("\n") {
        let (k, v) = line.split_once(" -> ").unwrap();
        replace_rules.insert(k, v.chars().next().unwrap());
    }

    (template, replace_rules)
}

fn insert(template: &str, replace_rules: &HashMap<&str, char>, n_iter: usize) -> String {
    let mut result = template.clone().to_string();

    for _ in 0..n_iter {
        let mut tmp = String::new();
        tmp.push(result.chars().next().unwrap());

        for i in 0..(result.len() - 1) {
            let pair = &result[i..(i + 2)];
            // the 1rst of the pair is the last of the next - just need to make sure
            // that the first char is inserted before the loop
            tmp.push(*replace_rules.get(pair).unwrap());
            tmp.push(pair.chars().nth(1).unwrap());
        }

        result = tmp;
    }

    result
}

fn count_chars(input: &str) -> HashMap<char, usize> {
    let mut result: HashMap<char, usize> = HashMap::new();

    for elem in input.chars() {
        let count = result.entry(elem).or_insert(0);
        *count += 1;
    }

    result
}

fn find_delta_min_max(input: &HashMap<char, usize>) -> usize {
    let mut min: usize = *input.values().next().unwrap();
    let mut max: usize = 0;

    for v in input.values() {
        if v > &max {
            max = *v;
        }
        if v < &min {
            min = *v;
        }
    }

    max - min
}

fn insert2(
    template: &str,
    replace_rules: &HashMap<&str, char>,
    n_iter: usize,
) -> HashMap<String, usize> {
    let mut pairs: HashMap<String, usize> = HashMap::new();

    // add a character at the end of the string. this creates an additional pair
    // which does not match any replace rules but is necessary carrying on
    // to get the count of the last character right.
    let mut template: String = template.to_string();
    template.push('*');

    for i in 0..(template.len() - 1) {
        let pair = template[i..(i + 2)].to_string();
        pairs.insert(pair, 1);
    }

    for _ in 0..n_iter {
        let mut tmp: HashMap<String, usize> = HashMap::new();

        for (pair, cnt) in &pairs {
            match replace_rules.get(&pair[..]) {
                Some(insert_char) => {
                    let mut p1: String = String::new();
                    p1.push(pair.chars().nth(0).unwrap());
                    p1.push(*insert_char);

                    let mut p2: String = String::new();
                    p2.push(*insert_char);
                    p2.push(pair.chars().nth(1).unwrap());

                    let count = tmp.entry(p1).or_insert(0);
                    *count += cnt;

                    let count = tmp.entry(p2).or_insert(0);
                    *count += cnt;
                }
                None => {
                    let count = tmp.entry(pair.clone()).or_insert(0);
                    *count += cnt;
                }
            }
        }
        pairs = tmp;
    }
    pairs
}

fn count_chars2(input: HashMap<String, usize>) -> HashMap<char, usize> {
    let mut result: HashMap<char, usize> = HashMap::new();

    for (pair, cnt) in input {
        // only count the first char of each pair (to avoid duplication)
        // error: on last elem!
        let count = result.entry(pair.chars().nth(0).unwrap()).or_insert(0);
        *count += cnt;
    }
    result
}

fn main() {
    let input = read_file();
    let (template, replace_rules) = parse_input(&input);

    // part 1
    let polymer10 = insert(template, &replace_rules, 10);
    let qty_min_max10 = find_delta_min_max(&count_chars(&polymer10));
    println!("part 1 qty min max delta: {}", qty_min_max10);

    // part 2
    let polymer40 = insert2(template, &replace_rules, 40);
    let qty_min_max40 = find_delta_min_max(&count_chars2(polymer40));
    println!("part 2 qty min max delta: {}", qty_min_max40);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_input() {
        let (template, replace_rules) = parse_input("NNCB\n\nCH -> B\nHH -> N\nCB -> H");
        assert_eq!(template, "NNCB");
        assert_eq!(replace_rules.get("CH").unwrap(), &'B');
        assert_eq!(replace_rules.get("HH").unwrap(), &'N');
        assert_eq!(replace_rules.get("CB").unwrap(), &'H');
    }

    fn get_test_input() -> (&'static str, HashMap<&'static str, char>) {
        parse_input(
            "NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C",
        )
    }

    #[test]
    fn test_insert_test1() {
        let (template, replace_rules) = get_test_input();
        assert_eq!(insert(template, &replace_rules, 1), "NCNBCHB");
    }
    #[test]
    fn test_insert_test2() {
        let (template, replace_rules) = get_test_input();
        assert_eq!(insert(template, &replace_rules, 2), "NBCCNBBBCBHCB");
    }
    #[test]
    fn test_insert_test3() {
        let (template, replace_rules) = get_test_input();
        assert_eq!(
            insert(template, &replace_rules, 3),
            "NBBBCNCCNBBNBNBBCHBHHBCHB"
        );
    }

    #[test]
    fn test_count() {
        let res = count_chars("NCNBCHB");
        assert_eq!(res.get(&'N').unwrap(), &(2 as usize));
        assert_eq!(res.get(&'C').unwrap(), &(2 as usize));
        assert_eq!(res.get(&'B').unwrap(), &(2 as usize));
        assert_eq!(res.get(&'H').unwrap(), &(1 as usize));
    }

    #[test]
    fn test_insert2() {
        let (template, replace_rules) = get_test_input();
        let res = insert2(template, &replace_rules, 3);

        // "NBBBCNCCNBBNBNBBCHBHHBCHB"
        assert_eq!(res.get("NB").unwrap(), &(4 as usize));
        assert_eq!(res.get("BB").unwrap(), &(4 as usize));
        assert_eq!(res.get("BC").unwrap(), &(3 as usize));
        assert_eq!(res.get("CN").unwrap(), &(2 as usize));
        assert_eq!(res.get("NC").unwrap(), &(1 as usize));
        assert_eq!(res.get("CC").unwrap(), &(1 as usize));
        assert_eq!(res.get("BN").unwrap(), &(2 as usize));
        assert_eq!(res.get("CH").unwrap(), &(2 as usize));
        assert_eq!(res.get("HB").unwrap(), &(3 as usize));
        assert_eq!(res.get("BH").unwrap(), &(1 as usize));
        assert_eq!(res.get("HH").unwrap(), &(1 as usize));
    }

    #[test]
    fn test_count2() {
        let (template, replace_rules) = get_test_input();
        let poly = insert2(template, &replace_rules, 3);
        let counts = count_chars2(poly);
        assert_eq!(counts.get(&'N').unwrap(), &(5 as usize));
        assert_eq!(counts.get(&'C').unwrap(), &(5 as usize));
        assert_eq!(counts.get(&'H').unwrap(), &(4 as usize));
        assert_eq!(counts.get(&'B').unwrap(), &(11 as usize));
    }

    #[test]
    fn test_find_delta_min_max() {
        let res = find_delta_min_max(&count_chars("NCNBCHB"));
        assert_eq!(res, 1 as usize);
    }
}
