use std::collections::HashSet;
use std::fs;

const BORDER_VAL: usize = 9;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

/// Convert the input to a grid with borders
fn to_grid(input: &str) -> Vec<Vec<usize>> {
    let mut grid: Vec<Vec<usize>> = Vec::new();

    for line in input.trim().split("\n") {
        let mut line_vec: Vec<usize> = line
            .chars()
            .map(|x| {
                x.to_string()
                    .parse()
                    .expect("failed converting char to number")
            })
            .collect();

        // add a border of elements to simplify neighbor checking on the borders
        line_vec.insert(0, BORDER_VAL);
        line_vec.push(BORDER_VAL);

        grid.push(line_vec);
    }

    let border_line: Vec<usize> = [BORDER_VAL].repeat(grid[0].len());
    grid.insert(0, border_line.clone());
    grid.push(border_line.clone());
    grid
}

/// Check if a certain grid element is a low point
///
/// # Arguments
///
/// * `grid` - the grid
/// * `i` - the row index; must satisfy 0 < i < len(row) (due to the borders)
/// * `j` - the column index; must satisfy 0 < j < len(column) (due to the borders)
fn is_lowpoint(grid: &Vec<Vec<usize>>, i: usize, j: usize) -> bool {
    if (i < 1) | (i >= grid.len()) {
        panic!("row index out of bounds");
    }
    if (j < 1) | (j >= grid[0].len()) {
        panic!("column index out of bounds");
    }

    if (grid[i][j] < grid[i - 1][j])
        & (grid[i][j] < grid[i + 1][j])
        & (grid[i][j] < grid[i][j - 1])
        & (grid[i][j] < grid[i][j + 1])
    {
        true
    } else {
        false
    }
}

/// Find the lowpoints in a grid
fn find_lowpoints(grid: &Vec<Vec<usize>>) -> Vec<(usize, usize)> {
    let mut low_points: Vec<(usize, usize)> = Vec::new();

    for i in 1..(grid.len() - 1) {
        for j in 1..(grid[0].len() - 1) {
            if is_lowpoint(grid, i, j) {
                low_points.push((i, j));
            }
        }
    }

    low_points
}

fn risk_level(grid: &Vec<Vec<usize>>, low_points: &Vec<(usize, usize)>) -> usize {
    let mut sum: usize = 0;
    for (i, j) in low_points {
        sum += grid[*i][*j] + 1;
    }
    sum
}

/// given a low point find all members of the basin
fn expand_basin(grid: &Vec<Vec<usize>>, low_point: (usize, usize)) -> HashSet<(usize, usize)> {
    let mut basin: HashSet<(usize, usize)> = HashSet::new();

    fn expand(grid: &Vec<Vec<usize>>, loc: (usize, usize), basin: &mut HashSet<(usize, usize)>) {
        if (grid[loc.0][loc.1] == 9) | (basin.contains(&loc)) {
            return;
        }
        (*basin).insert(loc);
        expand(grid, (loc.0 - 1, loc.1), basin); // up
        expand(grid, (loc.0 + 1, loc.1), basin); // down
        expand(grid, (loc.0, loc.1 - 1), basin); // left
        expand(grid, (loc.0, loc.1 + 1), basin); // right
    }

    expand(grid, low_point, &mut basin);

    basin
}

/// there are as many basins as there are low points
fn get_basin_sizes(grid: &Vec<Vec<usize>>, low_points: &Vec<(usize, usize)>) -> Vec<usize> {
    let mut basin_sizes: Vec<usize> = Vec::new();
    for low_point in low_points {
        basin_sizes.push(expand_basin(grid, *low_point).len());
    }

    basin_sizes
}

/// get three largest basins and return product of their sizes
fn basin_score(basin_sizes: &mut Vec<usize>) -> usize {
    basin_sizes.sort_unstable();
    let n = basin_sizes.len();

    let mut score: usize = 1;
    for i in (n - 3)..n {
        score *= basin_sizes[i];
    }

    score
}

fn main() {
    let input = read_file();
    let grid = to_grid(&input);

    // part 1
    let low_points = find_lowpoints(&grid);
    let risk_level = risk_level(&grid, &low_points);
    println!("part 1 risk level: {}", risk_level);

    // part 2
    let mut basin_sizes = get_basin_sizes(&grid, &low_points);
    let score = basin_score(&mut basin_sizes);
    println!("part 2 basin score: {}", score);
}

#[cfg(test)]
mod tests {
    use super::*;

    fn get_test_grid() -> Vec<Vec<usize>> {
        to_grid(
            "2199943210\n\
            3987894921\n\
            9856789892\n\
            8767896789\n\
            9899965678",
        )
    }

    #[test]
    fn test_to_grid() {
        assert_eq!(
            to_grid("123\n456"),
            vec![
                vec![BORDER_VAL; 5],
                vec![BORDER_VAL, 1, 2, 3, BORDER_VAL],
                vec![BORDER_VAL, 4, 5, 6, BORDER_VAL],
                vec![BORDER_VAL; 5]
            ]
        );
    }

    #[test]
    fn test_is_lowpoint() {
        let grid = get_test_grid();
        assert!(is_lowpoint(&grid, 1, 2));
        assert!(is_lowpoint(&grid, 1, 10));
        assert!(is_lowpoint(&grid, 3, 3));
        assert!(!is_lowpoint(&grid, 1, 1));
        assert!(!is_lowpoint(&grid, 2, 2));
        assert!(!is_lowpoint(&grid, 4, 4));
    }

    #[test]
    #[should_panic]
    fn test_is_lowpoint_panics1() {
        //  row index too small
        let grid = vec![vec![1, 2, 3], vec![4, 5, 6]];
        let _ = is_lowpoint(&grid, 0, 1);
    }
    #[test]
    #[should_panic]
    fn test_is_lowpoint_panics2() {
        // row index too large
        let grid = vec![vec![1, 2, 3], vec![4, 5, 6]];
        let _ = is_lowpoint(&grid, 2, 1);
    }
    #[test]
    #[should_panic]
    fn test_is_lowpoint_panics3() {
        // col index to small
        let grid = vec![vec![1, 2, 3], vec![4, 5, 6]];
        let _ = is_lowpoint(&grid, 1, 0);
    }
    #[test]
    #[should_panic]
    fn test_is_lowpoint_panics4() {
        // col index too large
        let grid = vec![vec![1, 2, 3], vec![4, 5, 6]];
        let _ = is_lowpoint(&grid, 1, 2);
    }

    #[test]
    fn test_find_lowpoints() {
        let grid = get_test_grid();
        assert_eq!(find_lowpoints(&grid), vec![(1, 2), (1, 10), (3, 3), (5, 7)]);
    }

    #[test]
    fn test_risk_level() {
        let grid = get_test_grid();
        let low_points = find_lowpoints(&grid);
        assert_eq!(risk_level(&grid, &low_points), 15);
    }

    #[test]
    fn test_expand_basin_top_left() {
        let grid = get_test_grid();
        let basin = expand_basin(&grid, (1, 2));

        assert_eq!(basin.len(), 3);
        assert!(basin.contains(&(1, 1)));
        assert!(basin.contains(&(1, 2)));
        assert!(basin.contains(&(2, 1)));
    }
    #[test]
    fn test_expand_basin_top_right() {
        let grid = get_test_grid();
        assert_eq!(expand_basin(&grid, (1, 10)).len(), 9);
    }
    #[test]
    fn test_expand_basin_middle() {
        let grid = get_test_grid();
        assert_eq!(expand_basin(&grid, (3, 3)).len(), 14);
    }
    #[test]
    fn test_expand_basin_bottom_right() {
        let grid = get_test_grid();
        assert_eq!(expand_basin(&grid, (5, 7)).len(), 9);
    }

    #[test]
    fn test_get_basin_sizes() {
        let grid = get_test_grid();
        let low_points = find_lowpoints(&grid);
        assert_eq!(get_basin_sizes(&grid, &low_points), vec![3, 9, 14, 9]);
    }

    #[test]
    fn test_basin_score() {
        assert_eq!(basin_score(&mut vec![3, 9, 14, 9]), 1134);
    }
}
