use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs;

fn read_file() -> String {
    fs::read_to_string("input.txt").expect("Something went wrong reading the file")
}

fn parse_input(input: &str) -> Vec<Line> {
    input
        .trim()
        .split("\n")
        .map(|x| Line::from_string(x))
        .collect()
}

struct Line {
    x1: isize,
    y1: isize,
    x2: isize,
    y2: isize,
}

impl Line {
    fn from_string(input: &str) -> Line {
        let nums: Vec<isize> = input
            .trim()
            .replace(" -> ", ",")
            .split(",")
            .map(|x| x.parse::<isize>().unwrap())
            .collect();

        if nums.len() != 4 {
            panic!();
        }

        Line {
            x1: nums[0],
            y1: nums[1],
            x2: nums[2],
            y2: nums[3],
        }
    }
}

struct Grid {
    fields: HashMap<(isize, isize), usize>,
}

impl Grid {
    fn new() -> Grid {
        Grid {
            fields: HashMap::new(),
        }
    }

    fn draw_hv_line(&mut self, line: &Line) {
        let x1 = line.x1.min(line.x2);
        let x2 = line.x1.max(line.x2);
        let y1 = line.y1.min(line.y2);
        let y2 = line.y1.max(line.y2);

        for x in x1..(x2 + 1) {
            for y in y1..(y2 + 1) {
                let count = self.fields.entry((x, y)).or_insert(0);
                *count += 1;
            }
        }
    }

    fn draw_diag_line(&mut self, line: &Line) {
        let dx: isize = match line.x1.cmp(&line.x2) {
            Ordering::Less => 1,
            Ordering::Greater => -1,
            Ordering::Equal => 0,
        };
        let dy: isize = match line.y1.cmp(&line.y2) {
            Ordering::Less => 1,
            Ordering::Greater => -1,
            Ordering::Equal => 0,
        };
        let mut x = line.x1;
        let mut y = line.y1;
        loop {
            let count = self.fields.entry((x, y)).or_insert(0);
            *count += 1;

            x += dx;
            y += dy;

            if (x < line.x1.min(line.x2)) | (x > line.x1.max(line.x2)) {
                // could also check for y, does not matter
                break;
            }
        }
    }

    fn count_dangerous_points(&self) -> usize {
        self.fields
            .values()
            .map(|&x| if x < 2 { 0 } else { 1 })
            .sum()
    }

    fn run_hv(&mut self, lines: &Vec<Line>) {
        for line in lines {
            if (line.x1 == line.x2) | (line.y1 == line.y2) {
                self.draw_hv_line(line);
            }
        }
    }

    fn run_all(&mut self, lines: &Vec<Line>) {
        for line in lines {
            if (line.x1 == line.x2) | (line.y1 == line.y2) {
                self.draw_hv_line(line);
            } else {
                self.draw_diag_line(line);
            }
        }
    }
}

fn main() {
    let input = read_file();
    let input_vec: Vec<Line> = parse_input(&input);

    // part 1
    let mut g = Grid::new();
    g.run_hv(&input_vec);
    println!(
        "part 1 number of dangerous points: {}",
        g.count_dangerous_points()
    );

    // part 2
    let mut g = Grid::new();
    g.run_all(&input_vec);
    println!(
        "part 2 number of dangerous points: {}",
        g.count_dangerous_points()
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_line_from_string() {
        let l = Line::from_string("0,9 -> 5,9");
        assert_eq!(l.x1, 0);
        assert_eq!(l.y1, 9);
        assert_eq!(l.x2, 5);
        assert_eq!(l.y2, 9);
    }

    #[test]
    fn test_grid_draw_hv_line() {
        let mut g = Grid::new();

        g.draw_hv_line(&Line::from_string("0,9 -> 5,9"));

        for x in 0..6 {
            assert_eq!(*g.fields.get(&(x, 9)).unwrap(), 1);
        }
    }
    #[test]
    fn test_grid_draw_diag_line() {
        let mut g = Grid::new();

        g.draw_diag_line(&Line::from_string("9,7 -> 7,9"));

        for (x, y) in [(9, 7), (8, 8), (7, 9)] {
            assert_eq!(*g.fields.get(&(x, y)).unwrap(), 1);
        }
    }

    fn get_test_input() -> Vec<Line> {
        let input = "0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2";
        parse_input(input)
    }

    #[test]
    fn test_count_dangerous_points_hv() {
        let mut g = Grid::new();
        g.run_hv(&get_test_input());

        assert_eq!(g.count_dangerous_points(), 5);
    }

    #[test]
    fn test_count_dangerous_points_all() {
        let mut g = Grid::new();
        g.run_all(&get_test_input());

        assert_eq!(g.count_dangerous_points(), 12);
    }
}
