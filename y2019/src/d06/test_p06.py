# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p06 import (
    parse_orbits,
    build_paths,
    count_direct_indirect_orbits,
    count_min_transfers,
)


orbits = """
COM)B
B)C
C)D
D)E
E)F
B)G
G)H
D)I
E)J
J)K
K)L
K)YOU
I)SAN
"""


def test_build_paths():
    assert build_paths(parse_orbits(orbits)) == {
        "B": ["COM"],
        "G": ["B", "COM"],
        "H": ["G", "B", "COM"],
        "C": ["B", "COM"],
        "D": ["C", "B", "COM"],
        "I": ["D", "C", "B", "COM"],
        "E": ["D", "C", "B", "COM"],
        "F": ["E", "D", "C", "B", "COM"],
        "J": ["E", "D", "C", "B", "COM"],
        "K": ["J", "E", "D", "C", "B", "COM"],
        "L": ["K", "J", "E", "D", "C", "B", "COM"],
        "YOU": ["K", "J", "E", "D", "C", "B", "COM"],
        "SAN": ["I", "D", "C", "B", "COM"],
    }


def test_count_direct_indirect_orbits():
    assert count_direct_indirect_orbits(build_paths(parse_orbits(orbits))) == 54


def test_count_min_transfers_one():
    assert count_min_transfers(build_paths(parse_orbits(orbits)), "YOU", "SAN") == 4


def test_count_min_transfers_two():
    assert count_min_transfers(build_paths(parse_orbits(orbits)), "SAN", "YOU") == 4
