#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 6"""

# stdlib imports
import copy
from typing import List, Dict

# 3rd party lib imports

# own stuff
import utils

S_COM = "COM"


def parse_orbits(input_data: str) -> Dict[str, List[str]]:
    """get the intcodes from the website"""
    lol = [elem.split(")") for elem in input_data.strip().split("\n")]
    # return as dict, where the RHS is the key
    return {k: [v] for v, k in lol}


def build_paths(orbits: Dict[str, List[str]]) -> Dict[str, List[str]]:
    """build the paths to COM for every planet"""
    paths = copy.deepcopy(orbits)

    while True:
        for key in paths:
            val = paths[key]
            if val[-1] != S_COM:
                val += paths[val[-1]]

        if all(val[-1] == S_COM for val in paths.values()):
            break

    return paths


def count_direct_indirect_orbits(paths: Dict[str, List[str]]) -> int:
    """count the number of direct and indirect orbits"""
    return sum(map(len, paths.values()))


def count_min_transfers(paths: Dict[str, List[str]], key1, key2):
    """count the minimal number of paths it takes to get from key1 to key2"""
    p1, p2 = paths[key1], paths[key2]
    shorter = min(len(p1), len(p2))

    for common_orb_index in range(1, shorter + 1):
        # start from COM and break out of loop once paths divert (are not same)
        if p1[-common_orb_index] != p2[-common_orb_index]:
            break

    # broke out of loop when diverging, so common orbit index is one less
    common_orb_index -= 1

    return len(p1) - common_orb_index + len(p2) - common_orb_index


def main():
    orbits = parse_orbits(utils.get_input(__file__))

    # part 1:
    paths = build_paths(orbits)
    n_orbits = count_direct_indirect_orbits(paths)
    print(f"part 1: sum of all orbits: {n_orbits}")

    n_min_transfers = count_min_transfers(paths, "YOU", "SAN")
    print(f"part 2: minimal transfers: {n_min_transfers}")


if __name__ == "__main__":
    main()
