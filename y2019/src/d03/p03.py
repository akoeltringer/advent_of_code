#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 3"""

# stdlib imports
from typing import List, Tuple, Set

# 3rd party lib imports

# own stuff
import utils


def get_wire_paths() -> List[List[str]]:
    """get the intcodes from the website"""
    return [row.split(",") for row in utils.get_input(__file__).strip().split("\n")]


def get_path_segment(
    start_x: int, start_y: int, instruction: str
) -> List[Tuple[int, int]]:
    """compute a path segment for a starting point (start_x, start_y) and a single
    instruction.
    """
    if instruction[0] == "U":
        dx, dy = 0, 1
    elif instruction[0] == "D":
        dx, dy = 0, -1
    elif instruction[0] == "R":
        dx, dy = 1, 0
    elif instruction[0] == "L":
        dx, dy = -1, 0
    else:
        raise ValueError(f"bad instruction: {instruction}")

    n_steps = int(instruction[1:])

    return [(start_x + dx * n, start_y + dy * n) for n in range(1, n_steps + 1)]


def get_path(instructions: List[str]) -> List[Tuple[int, int]]:
    """Compute an entire path for a list of instructions for a wire"""
    path: List[Tuple[int, int]] = []
    for instruction in instructions:
        start_x, start_y = path[-1] if len(path) > 0 else (0, 0)
        path += get_path_segment(start_x, start_y, instruction)

    return path


def manhattan_distance(x1: int, y1: int, x2: int = 0, y2: int = 0) -> int:
    """compute the manhattan distance between two points (x1, y1) and (x2, y2)"""
    return abs(x1 - x2) + abs(y1 - y2)


def get_intersections(
    path1: List[Tuple[int, int]], path2: List[Tuple[int, int]]
) -> Set[Tuple[int, int]]:
    """get the intersections between the two paths"""
    return set(path1).intersection(set(path2))


def compute_min_distance(
    path1: List[Tuple[int, int]], path2: List[Tuple[int, int]]
) -> int:
    """compute the minimum distance from (0, 0) of an intersection of two paths,
    as given by their moving instructions.
    """
    return min([manhattan_distance(*p) for p in get_intersections(path1, path2)])


def compute_min_steps(
    path1: List[Tuple[int, int]], path2: List[Tuple[int, int]]
) -> int:
    """compute the minimum number of combined steps to the intersections"""
    # the +2 is necessary b/c list indices in Python are 0-based (the first element
    # has n_steps = 1 but index 0)
    return (
        min([path1.index(i) + path2.index(i) for i in get_intersections(path1, path2)])
        + 2
    )


def main():
    instructions1, instructions2 = get_wire_paths()
    path1 = get_path(instructions1)
    path2 = get_path(instructions2)

    # part 1
    print(f"minimal distance: {compute_min_distance(path1, path2)}")

    # part 2
    print(f"minimal steps: {compute_min_steps(path1, path2)}")


if __name__ == "__main__":
    main()
