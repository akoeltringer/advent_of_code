# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p03 import (
    get_path_segment,
    get_path,
    manhattan_distance,
    compute_min_distance,
    compute_min_steps,
)


instruction_set_1 = (
    ["R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"],
    ["U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"],
)
instruction_set_2 = (
    ["R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"],
    ["U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"],
)


@pytest.mark.parametrize(
    "start_x,start_y,instruction,expected",
    [
        (0, 0, "U5", [(0, 1), (0, 2), (0, 3), (0, 4), (0, 5)]),
        (0, 0, "D5", [(0, -1), (0, -2), (0, -3), (0, -4), (0, -5)]),
        (0, 0, "R5", [(1, 0), (2, 0), (3, 0), (4, 0), (5, 0)]),
        (0, 0, "L5", [(-1, 0), (-2, 0), (-3, 0), (-4, 0), (-5, 0)]),
    ],
)
def test_get_path_segment(start_x, start_y, instruction, expected):
    assert get_path_segment(start_x, start_y, instruction) == expected


@pytest.mark.parametrize(
    "instructions,expected",
    [
        (["U1", "L1", "D2", "R1"], [(0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1)]),
    ],
)
def test_get_path(instructions, expected):
    assert get_path(instructions) == expected


@pytest.mark.parametrize(
    "x1,y1,expected",
    [(3, 3, 6), (-3, -3, 6), (3, -3, 6), (-3, 3, 6), (0, 0, 0)],
)
def test_manhattan_distance(x1, y1, expected):
    assert manhattan_distance(x1, y1) == expected


@pytest.mark.parametrize(
    "instructions1,instructions2,expected",
    [(*instruction_set_1, 159), (*instruction_set_2, 135)],
)
def test_compute_min_distance(instructions1, instructions2, expected):
    p1, p2 = get_path(instructions1), get_path(instructions2)
    assert compute_min_distance(p1, p2) == expected


@pytest.mark.parametrize(
    "instructions1,instructions2,expected",
    [(*instruction_set_1, 610), (*instruction_set_2, 410)],
)
def test_compute_min_steps(instructions1, instructions2, expected):
    p1, p2 = get_path(instructions1), get_path(instructions2)
    assert compute_min_steps(p1, p2) == expected
