#! -*- coding: utf-8 -*-

"""Intcode computer
"""

# stdlib imports
from typing import List, Tuple

# 3rd party lib imports

# own stuff

# number of instructions per op code
N_INSTRUCTIONS = {
    1: 4,
    2: 4,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 4,
    8: 4,
}


def parse_opcode(opcode: int) -> Tuple[int, int, int, int]:
    """parse the opcode and return (op_code, mode1, mode2, mode3)"""
    op_str = f"{opcode:05d}"
    return int(op_str[3:]), int(op_str[2]), int(op_str[1]), int(op_str[0])


def get_value(mode, param, int_codes):
    """handle parameter mode: return
    value from position if mode = 0 (position mode)
    value of index if mode = 1 (immediate mode)
    """
    if mode == 0:
        return int_codes[param]
    if mode == 1:
        return param

    raise ValueError


def intcode_compute(int_codes: List[int]) -> List[int]:
    """intcode computer"""
    int_codes = int_codes.copy()
    i = 0
    while i < len(int_codes):
        # parse op code and param modes
        op_code, mode1, mode2, mode3 = parse_opcode(int_codes[i])

        if op_code == 99:
            break

        # add (p02)
        if op_code == 1:
            idx1, idx2, idx3 = int_codes[(i + 1) : (i + 4)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            int_codes[idx3] = val1 + val2

        # multiply (p02)
        elif op_code == 2:
            idx1, idx2, idx3 = int_codes[(i + 1) : (i + 4)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            int_codes[idx3] = val1 * val2

        # input (p05)
        elif op_code == 3:
            idx1 = int_codes[(i + 1)]
            int_codes[idx1] = int(input("enter number: "))

        # output (p05)
        elif op_code == 4:
            idx1 = int_codes[(i + 1)]
            val1 = get_value(mode1, idx1, int_codes)
            print(val1)

        # jump-if-true (p05)
        elif op_code == 5:
            idx1, idx2 = int_codes[(i + 1) : (i + 3)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            if val1 != 0:
                i = val2
                continue  # do not alter instruction pointer anymore

        # jump-if-false (p05)
        elif op_code == 6:
            idx1, idx2 = int_codes[(i + 1) : (i + 3)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            if val1 == 0:
                i = val2
                continue  # do not alter instruction pointer anymore

        # less than (p05)
        elif op_code == 7:
            idx1, idx2, idx3 = int_codes[(i + 1) : (i + 4)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            int_codes[idx3] = 1 if val1 < val2 else 0

        # equals (p05)
        elif op_code == 8:
            idx1, idx2, idx3 = int_codes[(i + 1) : (i + 4)]
            val1 = get_value(mode1, idx1, int_codes)
            val2 = get_value(mode2, idx2, int_codes)
            int_codes[idx3] = 1 if val1 == val2 else 0

        else:
            raise ValueError(f"invalid opcode, was {op_code}")

        i += N_INSTRUCTIONS[op_code]

    return int_codes
