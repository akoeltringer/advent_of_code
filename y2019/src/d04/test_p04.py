# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p04 import (
    get_digits,
    has_adjacent_sames,
    digits_increasing,
    is_password_one,
    is_password_two,
)


@pytest.mark.parametrize(
    "number,expected",
    [
        (12345, [1, 2, 3, 4, 5]),
        (54321, [5, 4, 3, 2, 1]),
        (123, [1, 2, 3]),
        (543, [5, 4, 3]),
    ],
)
def test_get_digits(number, expected):
    assert get_digits(number) == expected


@pytest.mark.parametrize(
    "digits,expected",
    [
        ([1, 2, 3, 4, 5], False),
        ([5, 4, 3, 2, 1], False),
        ([1, 2, 2, 3, 4], True),
        ([5, 4, 3, 2, 2], True),
        ([5, 5, 4, 3, 2], True),
    ],
)
def test_has_adjacent_sames(digits, expected):
    assert has_adjacent_sames(digits) == expected


@pytest.mark.parametrize(
    "digits,expected",
    [
        ([1, 2, 3, 4, 5], True),
        ([5, 4, 3, 2, 1], False),
        ([1, 2, 2, 3, 4], True),
        ([5, 4, 3, 2, 2], False),
        ([1, 1, 1, 1, 0], False),
        ([0, 0, 0, 0, 1], True),
    ],
)
def test_digits_increasing(digits, expected):
    assert digits_increasing(digits) == expected


@pytest.mark.parametrize(
    "number,expected",
    [
        (111111, True),
        (223450, False),
        (123789, False),
        (122345, True),
        (111123, True),
        (135679, False),
    ],
)
def test_is_password_one(number, expected):
    assert is_password_one(number) == expected


@pytest.mark.parametrize(
    "number,expected",
    [
        (112233, True),
        (123444, False),
        (111122, True),
    ],
)
def test_is_password_two(number, expected):
    assert is_password_two(number) == expected
