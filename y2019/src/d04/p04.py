#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 4"""

# stdlib imports
from typing import Callable, List

# 3rd party lib imports

# own stuff


def get_digits(number: int) -> List[int]:
    """get the digits of number as a list of digits"""
    # index 0 is the last digit, so reverse for intuitive ordering
    return [int(i) for i in str(number)]


def has_adjacent_sames(digits: List[int]) -> bool:
    """check if at least two adjacent digits are the same"""
    return any([x == y for x, y in zip(digits[:-1], digits[1:])])


def digits_increasing(digits: List[int]) -> bool:
    """check if digits are monotonically increasing"""
    return all([x <= y for x, y in zip(digits[:-1], digits[1:])])


def is_password_one(password: int) -> bool:
    """check for valid password according to criteria of part 1"""
    digits = get_digits(password)
    return has_adjacent_sames(digits) and digits_increasing(digits)


def has_one_sequence_of_two_digit_number(digits: List[int]) -> bool:
    """has at least one sequence of "same" digits with a sequence length of two
    (not more) - part 2
    """
    indices = [
        e + 1
        for e, x, y in zip(range(len(digits) - 1), digits[:-1], digits[1:])
        if x != y
    ]
    indices = [0] + indices + [len(digits)]
    return any([up - lo == 2 for lo, up in zip(indices[:-1], indices[1:])])


def is_password_two(password: int) -> bool:
    """check for valid password according to criteria of part 2"""
    digits = get_digits(password)
    return has_one_sequence_of_two_digit_number(digits) and digits_increasing(digits)


def count_passwords_brute_force(
    lower: int, upper: int, validation_func: Callable
) -> int:
    """count passwords according to part 1"""
    return sum(validation_func(number) for number in range(lower, upper + 1))


def main():

    # part 1
    n1 = count_passwords_brute_force(245182, 790572, is_password_one)
    print(f"number passwords: {n1}")

    # part 2
    n2 = count_passwords_brute_force(245182, 790572, is_password_two)
    print(f"number passwords: {n2}")


if __name__ == "__main__":
    main()
