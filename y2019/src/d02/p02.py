#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 2"""

# stdlib imports
import itertools
from typing import List

# 3rd party lib imports

# own stuff
from y2019.src.intcode_computer import intcode_compute
import utils


def get_intcodes() -> List[int]:
    """get the intcodes from the website"""
    return [int(elem) for elem in utils.get_input(__file__).strip().split(",")]


def gravity_assist(int_codes: List[int], noun: int, verb: int) -> int:
    """compute the gravity assist value"""
    int_codes = int_codes.copy()
    int_codes[1] = noun
    int_codes[2] = verb

    return intcode_compute(int_codes)[0]


def main():
    int_codes = get_intcodes()

    # part 1
    print(f"value at pos 0: {gravity_assist(int_codes, 12, 2)}")

    # part 2
    for noun, verb in itertools.product(range(100), range(100)):
        if gravity_assist(int_codes, noun, verb) == 19690720:
            break

    # pylint: disable=undefined-loop-variable
    print(f"noun: {noun}, verb: {verb}, result: {noun * 100 + verb}")


if __name__ == "__main__":
    main()
