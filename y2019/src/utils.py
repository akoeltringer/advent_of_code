# -*- coding: utf-8 -*-

"""Common/utility functions used across many problems
"""

# stdlib imports
import os
import pathlib
from typing import Tuple

# 3rd party lib imports
from dotenv import load_dotenv, find_dotenv
import requests

# own stuff

# get from the browser
load_dotenv(find_dotenv())


def get_input(file_path: str) -> str:
    """get the input data from the specified url"""
    year, day = get_year_day(file_path)
    url = f"https://adventofcode.com/{year}/day/{day}/input"
    cookie = dict(session=os.environ["SESSION_ID"])
    res = requests.get(url, cookies=cookie)
    if res.status_code != 200:
        raise RuntimeError("request failed")

    return res.text


def get_year_day(path: str) -> Tuple[int, int]:
    """extract year and day info from a file path
    """
    p = pathlib.Path(path)
    day = int(p.parent.name[1:])
    year = int(p.parents[1].name[1:])
    return year, day
