#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 5"""

# stdlib imports
from typing import List

# 3rd party lib imports

# own stuff
from intcode_computer import intcode_compute
import utils


def get_intcodes() -> List[int]:
    """get the intcodes from the website"""
    return [int(elem) for elem in utils.get_input(__file__).strip().split(",")]


def main():
    int_codes = get_intcodes()

    # part 1: enter 1
    # part 2: enter 5
    intcode_compute(int_codes)


if __name__ == "__main__":
    main()
