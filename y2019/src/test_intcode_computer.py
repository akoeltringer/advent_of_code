# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .intcode_computer import intcode_compute


@pytest.mark.parametrize(
    "int_codes,expected",
    [
        # test cases from day 2
        ([1, 0, 0, 0, 99], [2, 0, 0, 0, 99]),
        ([2, 3, 0, 3, 99], [2, 3, 0, 6, 99]),
        ([2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801]),
        ([1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99]),
        # test cases day 5
        ([1002, 4, 3, 4, 33], [1002, 4, 3, 4, 99]),
        ([1101, 100, -1, 4, 0], [1101, 100, -1, 4, 99]),
    ],
)
def test_intcode_compute(int_codes, expected):
    assert intcode_compute(int_codes) == expected


@pytest.mark.parametrize(
    "int_codes,input_value,expected",
    [
        # test cases day 5 using input function
        ([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 8, [3, 9, 8, 9, 10, 9, 4, 9, 99, 1, 8]),
        ([3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8], 10, [3, 9, 8, 9, 10, 9, 4, 9, 99, 0, 8]),
        ([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 6, [3, 9, 7, 9, 10, 9, 4, 9, 99, 1, 8]),
        ([3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8], 10, [3, 9, 7, 9, 10, 9, 4, 9, 99, 0, 8]),
        ([3, 3, 1108, -1, 8, 3, 4, 3, 99], 8, [3, 3, 1108, 1, 8, 3, 4, 3, 99]),
        ([3, 3, 1108, -1, 8, 3, 4, 3, 99], 10, [3, 3, 1108, 0, 8, 3, 4, 3, 99]),
        ([3, 3, 1107, -1, 8, 3, 4, 3, 99], 6, [3, 3, 1107, 1, 8, 3, 4, 3, 99]),
        ([3, 3, 1107, -1, 8, 3, 4, 3, 99], 10, [3, 3, 1107, 0, 8, 3, 4, 3, 99]),
    ],
)
def test_intcode_compute_inputs(int_codes, input_value, expected, monkeypatch):
    monkeypatch.setattr("builtins.input", lambda _: input_value)
    assert intcode_compute(int_codes) == expected


@pytest.mark.parametrize(
    "int_codes,input_value,expected",
    [
        # test cases day 5 using input and print function
        ([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 0, "0\n"),
        ([3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9], 1, "1\n"),
        ([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 0, "0\n"),
        ([3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1], 1, "1\n"),
    ],
)
def test_intcode_compute_inputs_prints(
    int_codes, input_value, expected, monkeypatch, capsys
):
    monkeypatch.setattr("builtins.input", lambda _: input_value)
    intcode_compute(int_codes)
    assert capsys.readouterr().out == expected


larger_example = [
    3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
    1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
    999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
]


# test cases day 5 using input and print function
@pytest.mark.parametrize(
    "input_value,expected", [(7, "999\n"), (8, "1000\n"), (9, "1001\n")]
)
def test_intcode_compute_inputs_prints_large(
    input_value, expected, monkeypatch, capsys
):
    monkeypatch.setattr("builtins.input", lambda _: input_value)
    intcode_compute(larger_example)
    assert capsys.readouterr().out == expected
