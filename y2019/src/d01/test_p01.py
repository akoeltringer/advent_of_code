# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import pytest

# own stuff
from .p01 import fuel_requirement_excl, fuel_requirement_incl


@pytest.mark.parametrize("mass,fuel", [(12, 2), (14, 2), (1969, 654), (100756, 33583)])
def test_fuel_requirement_excl(mass, fuel):
    assert fuel_requirement_excl(mass) == fuel


@pytest.mark.parametrize("mass,fuel", [(12, 2), (14, 2), (1969, 966), (100756, 50346)])
def test_fuel_requirement_incl(mass, fuel):
    assert fuel_requirement_incl(mass) == fuel
