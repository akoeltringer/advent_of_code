#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Solution of Problem of Day 1"""

# stdlib imports
from typing import List

# 3rd party lib imports

# own stuff
import utils


def fuel_requirement_excl(mass: int) -> int:
    """calculate fuel requirement for a module with mass ``mass``, without
    taking into account the fuel requirement of the fuel itself.
    """
    return mass // 3 - 2


def fuel_requirement_incl(mass: int) -> int:
    """calculate fuel requirement for a module, taking into account the fuel
    requirement of the fuel itself.
    """
    fuel_req = fuel_requirement_excl(mass)
    return 0 if fuel_req < 0 else fuel_req + fuel_requirement_incl(fuel_req)


def get_input_parsed() -> List[int]:
    """get the input from the website and return it parsed in a suitable format"""
    return [int(elem) for elem in utils.get_input(__file__).strip().split("\n")]


def main():
    """Main entrypoint"""
    masses = get_input_parsed()

    # Part 1
    sum_fuel_req_excl = sum([fuel_requirement_excl(mass) for mass in masses])
    print(
        f"The sum of fuel requirements (excluding fuel itself) is: {sum_fuel_req_excl}"
    )

    # Part 2
    sum_fuel_req_incl = sum([fuel_requirement_incl(mass) for mass in masses])
    print(
        f"The sum of fuel requirements (including fuel itself) is: {sum_fuel_req_incl}"
    )


if __name__ == "__main__":
    main()
